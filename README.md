# ShutSentry  |  [Download (686KB)](https://gitlab.com/shutsentry/shutsentry/raw/master/Bin/ShutSentry.exe)  |  [Documentation](https://gitlab.com/shutsentry/shutsentry/blob/master/source/ShutSentry2Help.pdf)  |  [Linux Version](#linux-version)

### A virtual dead-man-switch for personal computers

ShutSentry is a virtual dead-man-switch that protects computers from unauthorized physical access, by presenting an on-screen challenge prompt at regular time intervals. If the correct password is not entered, the system is locked and/or optionally shut down after a specified delay.

The program can also react to other trigger events such as USB device detection or bluetooth status changes, and challenge for authentication in these cases as well. In this way, ShutSentry ensures that anyone attempting to gain unauthorized physical access to the machine will be locked out at the earliest possible opportunity.

See [full documentation](https://gitlab.com/shutsentry/shutsentry/blob/master/source/ShutSentry2Help.pdf) for further details.


#### Screenshot
![Screenshot](source/images/screenshots/ShutSentry2Screenshot.png)


#### [Download (686KB)](https://gitlab.com/shutsentry/shutsentry/raw/master/Bin/ShutSentry.exe)

| Release Info  |   |
| -------- | -------|  
| Version | 2.31 |  
| SHA256 Checksum | 370b998b17a07679a96c719155b5f8fc56435cb427291a4618f42c7f216f3d63 |  
| Compatible OS | Windows |  


-------------------------------------


## Linux Version

The Linux version of ShutSentry is a bash-script command line utility with an optional interactive interface. It has most of the same features as the Windows version and should run correctly on most modern popular Linux distributions, but has been tested most extensively with k/Ubuntu, Linux Mint, Manjaro, and MX Linux.  
  
The program file **shutsentry.sh** can be downloaded below, and then simply needs to be set as executable using either a graphical file manager or via the command line with:  
  
 `sudo chmod +x shutsentry.sh `
  
The program can then be run by typing **shutsentry.sh** and hitting Enter.  
  

  #### [Download shutsentry.sh](https://gitlab.com/shutsentry/shutsentry/-/raw/master/Src/Linux/shutsentry.sh?inline=false)

| Release Info  |   |
| -------- | -------|  
| Version  | 2.8-6 |  
| SHA256 Checksum  | bf8fa01b22706ec8b98f0824bebdcc47d32ae493d19b279371e60280422270ad |  
| Compatible OS | Linux |  



#### Screenshot
![Screenshot](source/images/screenshots/ScreenshotLinux.png)



