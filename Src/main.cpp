
#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif

#ifndef SHGFP_TYPE_CURRENT
#define SHGFP_TYPE_CURRENT 0
#endif

#define _WIN32_IE 0x0300
#define _WIN32_WINNT 0x0501
#define WINVER 0x502

#include <windows.h>
#include <tchar.h>
#include <stdlib.h>
#include <dbt.h>
#include <sstream>
#include <fstream>
#include <string>
#include <time.h>
#include <vector>
#include <algorithm>
#include "ShortSHA256.h"
#include <tlhelp32.h>
#include <richedit.h>
#include <wtsapi32.h>
#include <map>
#include <iterator>
#include <commctrl.h>
#include "bthdef.h"
#include "BluetoothAPIs.h"
using namespace std;

HWND hwnd,fwnd=0,cwnd=0,HelpWindow=0,mwnd=0,curwnd=0;
HWND StartButton,EditIntervalDuration,OptIntervalDoNothing,OptIntervalChallenge,EditFailKeystrokes,EditFailSeconds,EditChallengePrompt,OptUSBDoNothing,OptUSBChallenge,OptUSBShutDown,OptAudibleBeeps,OptFailKeystrokes,OptFailSeconds,HelpEdit1;
HWND PauseButton,EditPauseMinutes,LabelPause1,LabelPause2,LabelPause0,LabelPause4,EditPassword,OptAutoStart,OptAutoHide,HideButton,OptTestMode,OptNormal,OptDual,OptRandLoc,OptSystemCritical,OptCreateClones,EditContainer,QuitButton,HelpButton;
HWND OptFailAway,OptFailPresent,EditFailAwayPause,EditFailPresentCountdown,TextLabelGreen,OptFASD,OptFPSD,OptBlueChallenge,ButtonBlueChoose,OutList,ButtonBlueConfirm;
HFONT hf,hf2,hf3,hf4;
HBRUSH bluebrush,beigebrush,dbeigebrush,greybrush,greenbrush,orangebrush,backbrush,tempbrush;
HANDLE phnd=0,chnd=0,thnd=0,zhnd=0,chiefhandle=0,myhnd=0,ntdll=0,mtx=0;
long secx,secy,dproc,eproc,pbproc,sbproc,ecproc;
long timermode=0,timerinterval=1000,timervalue=0,challint=300000,shutint=30000,beepnote=0,cinterval=0,myiteration=0,intervalaction=0,failsecondsaction=0,failkeystrokesaction=0;
long maxkeystrokes=0,usbaction=0,pauseint=30,pausemode=0,testmode=0,incnames=0,guardmode=0,syscrit=0,procrole=0,quitstate=0,alreadyadmin=0,selfdelete=0,qstart=1,qnow=2,toggleon=0;
long autorun=0,randloc=0,lockfail=0,challengereason=0,autopaused=0,failaway=0,failpresent=0,failawaypause=0,failpresentcountdown=0,origshutint=0,bfound,bluetoothcheck=0;
RECT oldrect;
double hhorizontal,vvertical,fontratio;
string relayername,meprocname,saltedpasshash,savedsalt,authcode,selflauncher,challengeprompt,memprefs,selfdir,preffile,clonelist,mempass,chiefdir,btargetdevice,btargetname;
DWORD chiefid=0;

#define vv *vvertical
#define hh *hhorizontal
#define lh *hhorizontal + (secx * hhorizontal)
#define lv *vvertical + (secy * vvertical)

struct procset
{
    HWND w;
    HANDLE p;
    DWORD i;
    char c[255];
    char a[255];
    int active;
    int critical;
};
vector<procset> hlist;
map <HWND, COLORREF> bcolor;


LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WindowProcedureMB (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WindowProcedurePB (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WindowProcedureSB(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK EditorStaticProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WindowProcedureH (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK EnumChildProc(HWND hwnd, LPARAM lParam);
BOOL CALLBACK EnumChildProc2(HWND hwnd, LPARAM lParam);
LRESULT MakeCritical(bool opt);
BOOL EnablePriv(LPCSTR lpszPriv);
ULONG CheckCritical(HANDLE);
BOOL CALLBACK TrashWindows(HWND hwnd, LPARAM lParam);
void ZapWindows(int newmode=-1);
void sdown(long reason),challenge(long reason=0),success(),pcheck(),tcheck(),ccheck(),qcheck(),quitself(),loadprefs(int mode=0),RegisterUSB(),toggleactive(),WindowClose(),CallConsole(),gcheck(int reset=0),DeleteObs(),ncheck(),dobeeps(),ShowHelp(),spitclone(),bcheck();
void SavePrefs(int mode=0);
void kcheck(WPARAM k);
void submb(string mbt);
void bcheckthread(LPVOID n);
void phash(char *);
void determinerole(LPSTR lpszArgument);
void PauseOp(int mode);
void RestartProgram(string s);
long fcheck(),delorphans();
long receivedata(HWND fromwind, COPYDATASTRUCT * cdata);
long cloneself(long mode=0);
long CheckSus(HANDLE ch);
long CheckSus2(HANDLE ch);
void checkexinstance();
void GenerateWindows();
boolean dcheck(string df="");
string randname(),randnameempty(),inconame();
string wtoa (const std::wstring& wstr);
string pickfolder();
string randbytes();
string xunscramble(string s);
string xscramble(string s);
void ModifyExe(string e);
void CheckArgs(LPSTR);
void LookArgs();
void LockComp();

extern "C" long Base64Encode(char *input, char *output, int bytes);
extern "C" long Base64Decode(char *input, char *output);


TCHAR szClassName[255] = _T("CabinetWClass");

int WINAPI WinMain (HINSTANCE hThisInstance,HINSTANCE hPrevInstance,LPSTR lpszArgument,int nCmdShow)
{
    srand(time(0)*GetCurrentProcessId());

    char selfmodule[MAX_PATH] = {};
    GetModuleFileNameA(NULL, selfmodule, MAX_PATH);
    meprocname=selfmodule;
    strrchr(selfmodule,'.')[0]=0;
    preffile=selfmodule;
    preffile+=".ini";
    strrchr(selfmodule,'\\')[0]=0;
    selfdir=selfmodule;

    EnumChildWindows(NULL,&EnumChildProc2,0);

    MSG messages;
    WNDCLASSEX wincl;
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;
    wincl.style = CS_DBLCLKS;
    wincl.cbSize = sizeof (WNDCLASSEX);
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;
    wincl.cbClsExtra = 0;
    wincl.cbWndExtra = 0;
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;


    if (!RegisterClassEx (&wincl)){return 0;}

    RECT desktoprect={};
    SystemParametersInfo(SPI_GETWORKAREA,0,&desktoprect,0);


    hwnd = CreateWindowEx (0,szClassName,_T("ShutSentry"),WS_OVERLAPPED|WS_SYSMENU|WS_MINIMIZEBOX,(int)((double)((desktoprect.right-desktoprect.right*0.5)/2)),(int)((double)((desktoprect.bottom-desktoprect.bottom*0.83)/2)),(int)((double)desktoprect.right*0.5),(int)((double)desktoprect.bottom*0.83),HWND_DESKTOP,NULL,hThisInstance,NULL);
    mwnd=hwnd;

    CheckArgs(lpszArgument);
    LookArgs();

    if (procrole==3)
    {
        HWND hwnd3 = CreateWindowEx (0,szClassName,_T(""),WS_OVERLAPPEDWINDOW,-1000,-1000,1,1,mwnd,NULL,hThisInstance,NULL);
        ShowWindow(hwnd3,SW_SHOW);

        int tries=100,clonesneeded=2+rand()%3;

        while (clonesneeded and tries--)
        {
            if (cloneself(1)){clonesneeded--;}
        }

        quitself();
    }

    if (procrole==1)
    {
            if (syscrit){MakeCritical(true);}
            chiefhandle=OpenProcess(SYNCHRONIZE|PROCESS_QUERY_INFORMATION,true,chiefid);
            procset myinfo={};
            myinfo.w=hwnd;
            myinfo.p=myhnd;
            myinfo.i=GetCurrentProcessId();
            strcpy(myinfo.c,meprocname.c_str());
            authcode=randname();
            strcpy(myinfo.a,authcode.c_str());
            myinfo.active=1;
            myinfo.critical=syscrit;

            COPYDATASTRUCT cds={};
            cds.cbData=sizeof(procset);
            cds.lpData=&myinfo;

            int tries=60;
            DWORD dres=0;

            while (!SendMessageTimeout(cwnd,WM_COPYDATA,(WPARAM)hwnd,(LPARAM)&cds,SMTO_NORMAL,5000,&dres))
            {
                if (!WaitForSingleObject(chiefhandle,0) or !tries--){quitself();}
                Sleep(500);
            }

            timermode=1;
            timervalue=challint;
            SetTimer(hwnd,0,timerinterval,NULL);
            SetWindowText(hwnd,"");
            MoveWindow(hwnd,rand() % 1900,rand() % 1000,rand() % 1900,rand() % 1000,true);
    }


    if (!procrole)
    {
        hhorizontal=(double)(desktoprect.right-desktoprect.left)/(double)1920;
        vvertical=(double)(desktoprect.bottom-desktoprect.top)/(double)1040;
        HDC hdc=GetDC(hwnd);
        fontratio=(double)96/(double)GetDeviceCaps(hdc,LOGPIXELSY);
        double fratio = hhorizontal<vvertical ? hhorizontal : vvertical;
        fontratio*=fratio;
        if (fratio<=0.625){fontratio-=0.01;}
        int ps = -MulDiv(12*fontratio, GetDeviceCaps(hdc, LOGPIXELSY), 72);
        ReleaseDC(hwnd,hdc);
        hf=CreateFont(ps,0,0,0,400,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Arial");
        hf2=CreateFont(ps,0,0,0,FW_BOLD,0,true,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Arial");
        hf3=CreateFont(ps,0,0,0,FW_BOLD,0,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Arial");
        hf4=CreateFont(ps,0,0,0,FW_BOLD,true,0,0,ANSI_CHARSET,OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,PROOF_QUALITY,FF_DONTCARE,"Arial");
        bluebrush=CreateSolidBrush(RGB(200,200,255));
        beigebrush=CreateSolidBrush(RGB(233,236,216));
        dbeigebrush=CreateSolidBrush(RGB(133,136,116));
        greybrush=CreateSolidBrush(RGB(100,100,100));
        greenbrush=CreateSolidBrush(RGB(0,200,0));
        orangebrush=CreateSolidBrush(RGB(200,100,0));
        backbrush=(HBRUSH) COLOR_BACKGROUND;
        SetClassLongPtr(hwnd, GCLP_HBRBACKGROUND, (LONG)beigebrush);

        mtx=CreateMutex(NULL,FALSE,NULL);

        typedef BOOL ( __stdcall *ChangeWMFE )(IN HWND hwnd, IN UINT message, IN DWORD action, IN DWORD dnull);
        ChangeWMFE ChangeWindowMessageFilterEX = (ChangeWMFE)GetProcAddress(GetModuleHandle("user32"), "ChangeWindowMessageFilterEx");
        if (ChangeWindowMessageFilterEX){ChangeWindowMessageFilterEX(mwnd,WM_COPYDATA,1,0);}

        GenerateWindows();
        loadprefs();
        checkexinstance();
        SavePrefs();
        RegisterUSB();
        CallConsole();

        if (saltedpasshash.length()){SetWindowText(EditPassword,"\x0D[Stored]\x0A");}
        if (!(autorun and SendMessage(OptAutoHide,BM_GETCHECK,0,0))){ShowWindow (hwnd, SW_SHOW);}
        if(toggleon or autorun){toggleactive();}
        SetTimer(hwnd,0,timerinterval,NULL);

    }


    while (GetMessage (&messages, NULL, 0, 0))
    {
        if(!IsDialogMessage(hwnd, &messages))
       {
            TranslateMessage(&messages);
            DispatchMessage(&messages);
       }
    }
    return messages.wParam;
}


LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_QUERYENDSESSION:
            if (!procrole){SavePrefs();}
            sdown(0);
            return 1;

        case WM_ENDSESSION:
            sdown(0);
            return 0;
            break;


        case WM_DESTROY:
            if (hwnd==EditContainer){break;}
            PostQuitMessage (0);
            break;

        case WM_CLOSE:
            if (hwnd==EditContainer){return 0;}
            WindowClose();
            return 0;

        case WM_SETFOCUS:
            if (fwnd){SetForegroundWindow(fwnd);return 0;}

        case WM_DEVICECHANGE:
            if (wParam==DBT_DEVICEARRIVAL or wParam==DBT_DEVICEREMOVECOMPLETE)
            {
                static DWORD lastusbevent=0;
                long curnow=GetTickCount();
                if(curnow < lastusbevent + 30000){break;}
                lastusbevent=curnow;

                if (timermode and !pausemode)
                {
                    if (usbaction==1)
                    {Sleep(4000);challenge();}
                    if (usbaction==2)
                    {sdown(1);}
                }
            }
            break;

        case WM_WTSSESSION_CHANGE:

            if (wParam==8 and fwnd)
            {

                if (GetForegroundWindow()!=fwnd){SetForegroundWindow(fwnd);}
            }


            if (wParam==8 and lockfail==2)
            {
                Sleep(2000);
                if (timervalue>shutint){timervalue=shutint;}
                challenge();
                //break;
            }


            if (wParam==8 and autopaused==1 and lockfail==1)
            {
                lockfail=0;
                PauseOp(0);
                autopaused=0;
                lockfail=0;
                Sleep(2000);
                challenge();
            }



            break;


        case WM_CTLCOLORSTATIC:
            {
                if ((HWND)lParam==LabelPause0 or (HWND)lParam==LabelPause4)
                {
                    SetTextColor((HDC)wParam,RGB(255,255,255));
                    if (pausemode){SetBkColor((HDC)wParam,RGB(200,100,0));return (LRESULT)orangebrush;}
                    if (timermode){SetBkColor((HDC)wParam,RGB(0,200,0));return (LRESULT)greenbrush;}
                    SetBkColor((HDC)wParam,RGB(100,100,100));return (LRESULT)greybrush;
                }

                if (bcolor[(HWND)lParam])
                {
                    SetBkColor((HDC)wParam,bcolor[(HWND)lParam]);

                    if (bcolor[(HWND)lParam]==-1)
                    {
                        SetBkMode((HDC)wParam,TRANSPARENT);
                        return (LRESULT)GetStockObject(NULL_BRUSH);
                    }
                    DeleteObject(tempbrush);
                    tempbrush=CreateSolidBrush(bcolor[(HWND)lParam]);
                    return (LRESULT)tempbrush;
                }



                SetBkColor((HDC)wParam,RGB(233,236,216));
                return (LRESULT)beigebrush;
            }

        case WM_CTLCOLOREDIT:
        {
            if ((HWND)lParam==EditPassword)
            {
                char buffer[255];
                GetWindowText(EditPassword,buffer,sizeof(buffer));
                if (buffer[0]==13)
                {
                    SetTextColor((HDC)wParam,RGB(150,150,150));
                    return (LRESULT) GetStockObject(DC_BRUSH);
                }
            }
            break;
        }


        case WM_COMMAND:
        {
            if (HIWORD(wParam)==BN_CLICKED and ((ButtonBlueConfirm==(HWND)lParam)))
            {
                char buffer[1000]={};
                int i=ListView_GetNextItem(OutList,-1,LVNI_SELECTED);
                if (i==-1){break;}
                ListView_GetItemText(OutList,i,0,(LPTSTR)buffer,MAX_PATH);
                btargetname=string(buffer);
                ListView_GetItemText(OutList,i,1,(LPTSTR)buffer,MAX_PATH);
                btargetdevice=string(buffer);
                ListView_DeleteAllItems(OutList);

                LVITEM lvi={};
                lvi.iItem=ListView_GetItemCount(OutList)+2;

                int ni=ListView_InsertItem(OutList,&lvi);
                ListView_SetItemText(OutList,ni,0,(LPSTR)btargetname.c_str());
                ListView_SetItemText(OutList,ni,1,(LPSTR)btargetdevice.c_str());
                ShowWindow(ButtonBlueConfirm,SW_HIDE);
                SetWindowText(ButtonBlueChoose,"Change");

            }

            if (HIWORD(wParam)==BN_CLICKED and ((ButtonBlueChoose==(HWND)lParam)))
            {
                if (IsWindowVisible(ButtonBlueConfirm))
                {
                    SetWindowText(ButtonBlueChoose,"Change");
                    ShowWindow(ButtonBlueConfirm,SW_HIDE);
                    ListView_DeleteAllItems(OutList);
                    LVITEM lvi={};
                    lvi.iItem=ListView_GetItemCount(OutList)+2;
                    int ni=ListView_InsertItem(OutList,&lvi);
                    ListView_SetItemText(OutList,ni,0,(LPSTR)btargetname.c_str());
                    ListView_SetItemText(OutList,ni,1,(LPSTR)btargetdevice.c_str());
                }
                else
                {
                    SetWindowText(ButtonBlueChoose,"Cancel");
                    int * n=new int(1);
                    CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)bcheckthread,n,0,NULL);
                }
            }

            if (HIWORD(wParam)==BN_CLICKED and ((StartButton==(HWND)lParam)))
            {toggleactive();return 0;}

            if (HIWORD(wParam)==BN_CLICKED and ((PauseButton==(HWND)lParam)))
            {PauseOp(!pausemode);return 0;}

            if (HIWORD(wParam)==BN_CLICKED and ((HideButton==(HWND)lParam)))
            {ZapWindows();return 0;ShowWindow(hwnd,SW_HIDE);return 0;}

            if (HIWORD(wParam)==BN_CLICKED and ((QuitButton==(HWND)lParam)))
            {WindowClose();return 0;}

            if (HIWORD(wParam)==BN_CLICKED and (HWND)lParam==HelpButton)
            {ShowHelp();break;}

            if (HIWORD(wParam)==BN_CLICKED and OptUSBChallenge==(HWND)lParam and SendMessage(OptUSBChallenge,BM_GETCHECK,0,0))
            {SendMessage(OptUSBShutDown,BM_SETCHECK,BST_UNCHECKED,0);}

            if (HIWORD(wParam)==BN_CLICKED and OptUSBShutDown==(HWND)lParam and SendMessage(OptUSBShutDown,BM_GETCHECK,0,0))
            {SendMessage(OptUSBChallenge,BM_SETCHECK,BST_UNCHECKED,0);}

            if (HIWORD(wParam)==BN_CLICKED and OptAutoStart==(HWND)lParam)
            {
                string randkeyname=SHA256(meprocname.c_str());
                HKEY hkey = NULL;
                RegCreateKey(HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", &hkey);
                if (SendMessage(OptAutoStart,BM_GETCHECK,0,0))
                {
                    char buffer[1000]={};
                    strcpy(buffer,"\"");
                    strcat(buffer,meprocname.c_str());
                    strcat(buffer,"\" autorun");
                    RegSetValueEx(hkey, randkeyname.c_str(), 0, REG_SZ, (BYTE *)buffer, strlen(buffer)+1);
                }
                else
                {RegDeleteValue(hkey,randkeyname.c_str());}
                return 0;
            }
            break;
        }


        case WM_TIMER:
        {tcheck();break;}

        case WM_COPYDATA:
        {
            HRESULT rcd=receivedata((HWND)wParam,(COPYDATASTRUCT *)lParam);
            return rcd;
            break;
        }

        default:
            return DefWindowProc (hwnd, message, wParam, lParam);

    }

    return DefWindowProc (hwnd, message, wParam, lParam);
}


void challenge(long reason)
{
    challengereason=reason;
    if (timermode==2){return;}
    if (quitstate or testmode==2){return;}
    timermode=0;
    string mbtitle=randnameempty();
    CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)submb,&mbtitle,0,NULL);
    timermode=2;
    if (lockfail!=2 or !failpresentcountdown){timervalue=shutint;}
    MessageBox(HWND_DESKTOP,(LPCSTR)challengeprompt.c_str(),(LPCSTR)mbtitle.c_str(),MB_OK|MB_SYSTEMMODAL);
    if (timermode==2){sdown(2);}

}

void sdown(long reason=0)
{
    timermode=0;


    if (reason==12 and challengereason==1 and failaway==1 and lockfail==0)
    {
        success();
        lockfail=1;
        autopaused=1;
        pausemode=600000;
        PauseOp(1);
        pausemode=failawaypause ? failawaypause : 600001;
        LockComp();
        return;
    }


    if (failpresent==1)
    {
        if (timervalue<=0 and failpresentcountdown and lockfail==2){}
        else
        {
            SendMessage(fwnd,WM_CLOSE,0,0);
            fwnd=NULL;
            timermode=1;
            LockComp();
            if (lockfail!=2){timervalue=failpresentcountdown;}
            lockfail=2;
            return;
        }

    }


    if (syscrit){MakeCritical(false);}
    if (testmode==2){return;}
    if (testmode or testmode)
    {
        testmode=2;
        EnableWindow(hwnd,false);
        if(fwnd){SendMessage(fwnd,WM_CLOSE,0,0);}
        MoveWindow(hwnd,-500,-500,0,0,true);
        ShowWindow(hwnd,SW_HIDE);
        MessageBox(NULL,(LPCSTR)"At this point your computer would automatically shut down.\n\nHowever since ShutSentry is currently operating in test mode, the program will simply close instead.","ShutSentry",MB_OK|MB_SYSTEMMODAL);
        quitstate = procrole ? qnow : qstart;
    }
    else
    {
        if (!procrole){SavePrefs();}
        EnablePriv(SE_SHUTDOWN_NAME);
        while (1)
        {
            if(ExitWindowsEx(EWX_SHUTDOWN|EWX_FORCE, 0L)){break;}
            Sleep(500);
        }
        exit(0);
    }



}

void success()
{
    timervalue=challint;
    timermode=1;
    lockfail=0;
    SendMessage(fwnd,WM_CLOSE,0,0);
    fwnd=NULL;
}


void kcheck(WPARAM k)
{
    static string keyseq="";

    keyseq+=char(k);

    for (uint x=1;x<=keyseq.length();x++)
    {
        string checkseq=keyseq.substr(keyseq.length()-x,x)+savedsalt;

        for (int z=0;z<1000;z++)
        {
            checkseq=SHA256(checkseq.c_str());
        }

        if (SHA256(checkseq.c_str())==saltedpasshash)
        {
            keyseq.clear();
            mempass=checkseq;
            success();
            break;
        }
    }

    if (keyseq.length()>=(size_t)maxkeystrokes and maxkeystrokes and failkeystrokesaction)
    {sdown(4);}
}


void pcheck()
{
    if (!timermode){return;}
    if (quitstate){return;}

    if (!procrole)
    {
        for (uint x=0;x<hlist.size();x++)
        {
            if (hlist[x].active)
            {
                if (!WaitForSingleObject(hlist[x].p,0))
                {sdown(5);}

                if (CheckSus2(hlist[x].p))
                {hlist[x].active=-1;sdown(22);}

                if (syscrit and hlist[x].critical and !CheckCritical(hlist[x].p))
                {sdown(7);}
            }
        }
    }


    if (procrole)
    {
        if (!WaitForSingleObject(chiefhandle,0)){spitclone();sdown(8);}
        if (syscrit and !CheckCritical(chiefhandle)){spitclone();sdown(9);}
        if (CheckSus2(chiefhandle)){spitclone();sdown(25);}
    }

    if (syscrit and !CheckCritical(GetCurrentProcess()))
    {if (procrole){spitclone();}sdown(11);}
}


void tcheck()
{

    fcheck();
    pcheck();
    dcheck();
    qcheck();
    gcheck();
    bcheck();

    if (procrole){return;}

    timervalue-=timerinterval;

    if (pausemode)
    {
        pausemode-=timerinterval;
        if (pausemode & 1)
        {pausemode=60001;}

        if (timermode==1 and timervalue<=5000 and pausemode==5000 and intervalaction and beepnote)
        {CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)dobeeps,NULL,0,NULL);}
        if (pausemode<=0 and lockfail==1){failpresent=0;sdown(121);}
        else if (pausemode<=0){PauseOp(0);}
        return;
    }


    if (timermode==1 and timervalue and timervalue==5000 and intervalaction and beepnote and !lockfail)
    {CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)dobeeps,NULL,0,NULL);}

    if (timermode==1 and timervalue<=0 and intervalaction and lockfail!=2)
    {if (intervalaction==2){intervalaction=0;}challenge(1);}

    if (timermode==2 and timervalue<=0 and failsecondsaction and shutint)
    {sdown(12);}
    else if (lockfail==2 and timervalue<=0 and failpresent and failpresentcountdown)
    {sdown(120);}

}



string randname()
{
    string s;

    int z=(rand()%11)+25;
    for (int x=1;x<z;x++)
    {
        unsigned letter=(rand() % 26)+97;
        s+=letter;
    }
    return s;
}

string randbytes()
{
    string s;

    long z = (rand()%1000)*(1000)+(rand()%1000);
    for (int x=1;x<z;x++)
    {
        unsigned letter=(rand() % 26)+97;
        s+=letter;
    }
    return s;
}



string randnameempty()
{
    string s;

    int z=(rand()%11)+19;
    for (int x=1;x<z;x++)
    {
        unsigned letter=(rand() % 2);
        s+=letter==0 ? "\t" : "\n";
    }
    return s;
}


string wtoa (const std::wstring& wstr)
{
   return (std::string(wstr.begin(), wstr.end()));
}


long receivedata(HWND fromwind, COPYDATASTRUCT * cdata)
{
    if (procrole and fromwind==cwnd)
    {
        if (cdata->dwData==3)
        {
            char * ac=(char*)(cdata->lpData);
            if (strcmp(ac,authcode.c_str()))
            {sdown(14);return 1;}
            quitstate=qnow;
            return 11;
        }
    }

    if (!procrole and cdata->dwData==33)
    {
        if (string((char*)(cdata->lpData))!=meprocname){return 1;}
        if (quitstate or testmode==2){return 0;}
        if(!IsWindowVisible(mwnd)){ZapWindows();}
        ShowWindow(hwnd,SW_SHOW);
        ShowWindow(hwnd,SW_RESTORE);
        SetForegroundWindow(hwnd);
        if(timermode==1 and !intervalaction){intervalaction=2;}
        if (timermode==1 and intervalaction){timervalue=2000;}

        return 33;
    }



    if (!procrole and cdata->dwData==0)
    {
        procset tps=(*(procset*)(cdata->lpData));
        tps.p=OpenProcess(SYNCHRONIZE|PROCESS_QUERY_INFORMATION,true,tps.i);
        hlist.push_back(tps);
        return 1;
    }

    return 1;


}

long cloneself(long mode)
{
        if (procrole and !dcheck()){return 0;}
        if (!mode){mode=procrole;}

        string argz="";
        char ch[256]={};

        sprintf(ch," %li ",mode);
        argz+="?procrole*";argz+=ch;
        ltoa((long)chiefid,ch,10);
        argz+="?chiefid*"+string(ch);
        sprintf(ch," %li",(long)cwnd);
        argz+="?chiefwindow*"+string(ch);
        if (syscrit){argz+="?syscrit";}
        if (testmode){argz+="?testmode";}
        if (randloc){argz+="?randloc";}
        argz+="?selflauncher*"+meprocname;
        argz+="?chiefdir*"+selfdir;

        STARTUPINFO si={};
        si.cb = sizeof(si);
        PROCESS_INFORMATION pi={};
        si.dwFlags=STARTF_FORCEOFFFEEDBACK;

        if (mode==3)
        {
            string sh=meprocname;
            sh+=" "+argz;

            if (!CreateProcess(NULL, (char*)sh.c_str(), NULL, NULL, true, 0, NULL, NULL, &si, &pi)){return 0;}
            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
            return true;
        }


        if (mode==1)
        {
            argz+="?selfdelete";
            string sh = randloc ? pickfolder() : selfdir;
            sh+="\\"+inconame();
            if(!CopyFile(meprocname.c_str(),sh.c_str(),false)){return 0;}
            string shargs=sh;
            int n=shargs.rfind('.');
            shargs=shargs.substr(0,n);
            shargs+="args.dat";
            FILE * fargs = fopen(shargs.c_str(),"w");
            if (fargs==NULL){DeleteFile(sh.c_str());DeleteFile(shargs.c_str());return 0;}
            fputs(argz.c_str(),fargs);
            fclose(fargs);
            ModifyExe(sh);
            string shname=sh;
            if (!CreateProcess(NULL, (char*)sh.c_str(), NULL, NULL, true, 0, NULL, NULL, &si, &pi)){DeleteFile(shname.c_str());DeleteFile(shargs.c_str());return 0;}
            return true;
        }

        return true;

}


void quitself()
{
    timermode=0;
    if (procrole and !dcheck()){return;}
    if (!procrole and !fcheck()){return;}
    if (!procrole){curwnd=0;SavePrefs();}
    if (syscrit){MakeCritical(false);}
    if (!procrole){DeleteObs();}
    if (selfdelete){RestartProgram("?deleteme");}
    exit(0);
}


boolean dcheck(string df)
{
    static string dfile="";
    if (df!=""){dfile=df;}

    if (dfile.length())
    {
        if (DeleteFile(dfile.c_str()))
        {
            dfile="";
            return true;
        }
        else if(INVALID_FILE_ATTRIBUTES == GetFileAttributes(dfile.c_str()) && GetLastError()==ERROR_FILE_NOT_FOUND)
        {
            dfile="";
            return true;
        }
        else
        {
            return false;
        }
    }

    return true;

}


LRESULT CALLBACK WindowProcedureMB (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    static int edown=0;

    switch (message)
    {

        case WM_GETDLGCODE:
          return DLGC_WANTALLKEYS;

        case WM_KEYDOWN:
            if (wParam==VK_SPACE){return 0;}
            if (wParam==VK_RETURN){edown=1;}
            break;

        case WM_KEYUP:
        {
            if (!edown and wParam==VK_RETURN){return 0;}
            if (wParam==VK_RETURN or wParam==VK_ESCAPE or wParam==VK_SPACE)
            {sdown(3);return 0;}
            edown=0;
            return 0;
        }

        case WM_CHAR:
        {
            kcheck(wParam);
            return 0;
        }

        default:
            return CallWindowProc((WNDPROC)eproc,hwnd,message,wParam,lParam);

    }

     return CallWindowProc((WNDPROC)eproc,hwnd,message,wParam,lParam);
}


void submb(string mbt)
{
        HWND mh=0;
        do
        {
            mh=FindWindow(NULL,(LPCSTR)mbt.c_str());
            Sleep(1);
        } while (mh==0);

        HWND mok=0;
        do
        {
          mok=GetDlgItem(mh,2);
          Sleep(1);
        } while (mok==0);
        eproc=SetWindowLong(mok, GWL_WNDPROC, (LONG)WindowProcedureMB);
        SetWindowText(mh,"");
        fwnd=mh;
}

LRESULT CALLBACK WindowProcedurePB (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
        case WM_SETFOCUS:
        {
            SetWindowText(hwnd,"");
            SendMessage(hwnd,EM_SETPASSWORDCHAR,42,0);
            SetWindowLong(EditPassword, GWL_STYLE, GetWindowLong(EditPassword,GWL_STYLE) &~ ES_CENTER);
            break;
        }


        case WM_KILLFOCUS:
        {
            if (timermode){break;}
            char buffer[255];
            GetWindowText(hwnd,buffer,sizeof(buffer));
            if (strlen(buffer)){phash(buffer);}
            if (saltedpasshash.length()){SetWindowText(hwnd,"\x0D[Stored]");}
            else {SetWindowText(hwnd,"\x0D[Needed]");}
            SendMessage(hwnd,EM_SETPASSWORDCHAR,0,0);
            SetWindowLong(EditPassword, GWL_STYLE, GetWindowLong(EditPassword,GWL_STYLE) | ES_CENTER);
            break;
        }

        case WM_KEYUP:
        {
            if (wParam==VK_RETURN)
            {
                char buffer[255];
                GetWindowText(hwnd,buffer,sizeof(buffer));
                if (strlen(buffer)){SetFocus(StartButton);return 0;}
            }
            break;
        }


        default:
             return CallWindowProc((WNDPROC)pbproc,hwnd,message,wParam,lParam);

    }
    return CallWindowProc((WNDPROC)pbproc,hwnd,message,wParam,lParam);
}


void phash(char * pword)
{
    string password=pword;

    char salt[32];
    for (uint x=0;x<sizeof(salt);x++)
    {salt[x]=rand();}

    savedsalt=SHA256(salt,sizeof(salt));
    password+=savedsalt;
    saltedpasshash=SHA256(password.c_str());

    for (int z=0;z<1000;z++)
    {saltedpasshash=SHA256(saltedpasshash.c_str());}
}



void loadprefs(int mode)
{
    char buffer[MAX_PATH*6] = {};
    FILE * fp;
    stringstream is(memprefs);
    string token;
    string decprefs;

    decprefs=memprefs;

    if (!mode)
    {
        fp=fopen(preffile.c_str(),"r+");
        if (fp==NULL){return;}
        fseek(fp,0,SEEK_END);
        long ft=ftell(fp)+1;
        ft*=2;
        fseek(fp,0,SEEK_SET);
        char * fullbuffer = new char[ft];
        memset(fullbuffer,0,ft);
        fgets(fullbuffer,ft,fp);
        decprefs=xunscramble(string(fullbuffer));
        delete[] fullbuffer;
        fclose(fp);
    }


    stringstream dprefs(decprefs);

    while (getline(dprefs,token,'\n'))
    {

        if (mode or 1){strncpy(buffer,token.c_str(),MAX_PATH*5);}

        char* prefval=strchr(buffer,char(27));
        if (prefval==NULL){return;}
        prefval++[0]=0;
        char* nline=strchr(prefval,char(10));
        if (nline!=NULL){nline[0]=0;}

        string sprefval=prefval;
        string sprefname=buffer;
        long pval=strtol(prefval,NULL,10);

        if (sprefname=="Salt")
        {savedsalt=prefval;}

        if (sprefname=="Saltedpasshash")
        {saltedpasshash=prefval;}

        if (sprefname=="EditIntervalDuration")
        {SendMessage(EditIntervalDuration,WM_SETTEXT,0,(LPARAM)prefval);}

        if (sprefname=="OptIntervalDoNothing")
        {SendMessage(OptIntervalDoNothing,BM_SETCHECK,pval,0);}

        if (sprefname=="OptIntervalChallenge")
        {SendMessage(OptIntervalChallenge,BM_SETCHECK,pval,0);}

        if (sprefname=="OptUSBDoNothing")
        {SendMessage(OptUSBDoNothing,BM_SETCHECK,pval,0);}

        if (sprefname=="OptUSBChallenge")
        {SendMessage(OptUSBChallenge,BM_SETCHECK,pval,0);}

        if (sprefname=="OptIntervalDoNothing")
        {SendMessage(OptIntervalDoNothing,BM_SETCHECK,pval,0);}

        if (sprefname=="OptUSBShutDown")
        {SendMessage(OptUSBShutDown,BM_SETCHECK,pval,0);}

        if (sprefname=="EditChallengePrompt")
        {
            string ecp=prefval;
            replace(ecp.begin(),ecp.end(),char(27),char(13));
            replace(ecp.begin(),ecp.end(),char(255),char(10));
            SendMessage(EditChallengePrompt,WM_SETTEXT,0,(LPARAM)ecp.c_str());
        }

        if (sprefname=="EditFailSeconds")
        {SendMessage(EditFailSeconds,WM_SETTEXT,0,(LPARAM)prefval);}

        if (sprefname=="EditPauseMinutes")
        {SendMessage(EditPauseMinutes,WM_SETTEXT,0,(LPARAM)prefval);}

        if (sprefname=="EditFailKeystrokes")
        {SendMessage(EditFailKeystrokes,WM_SETTEXT,0,(LPARAM)prefval);}

        if (sprefname=="OptFailSeconds")
        {SendMessage(OptFailSeconds,BM_SETCHECK,pval,0);}

        if (sprefname=="OptFailKeystrokes")
        {SendMessage(OptFailKeystrokes,BM_SETCHECK,pval,0);}

        if (sprefname=="OptAudibleBeeps")
        {SendMessage(OptAudibleBeeps,BM_SETCHECK,pval,0);}

        if (sprefname=="OptAutoHide")
        {SendMessage(OptAutoHide,BM_SETCHECK,pval,0);}

        if (sprefname=="OptAutoStart")
        {SendMessage(OptAutoStart,BM_SETCHECK,pval,0);}

        if (sprefname=="OptTestMode")
        {SendMessage(OptTestMode,BM_SETCHECK,pval,0);}

        if (sprefname=="OptCreateClones")
        {SendMessage(OptCreateClones,BM_SETCHECK,pval,0);}

        if (sprefname=="OptRandLoc")
        {SendMessage(OptRandLoc,BM_SETCHECK,pval,0);}

        if (sprefname=="OptSystemCritical")
        {SendMessage(OptSystemCritical,BM_SETCHECK,pval,0);}

        if (sprefname=="OptFailAway" and pval)
        {SendMessage(OptFailAway,BM_SETCHECK,pval,0);SendMessage(OptFASD,BM_SETCHECK,!pval,0);}

        if (sprefname=="OptFailPresent" and pval)
        {SendMessage(OptFailPresent,BM_SETCHECK,pval,0);SendMessage(OptFPSD,BM_SETCHECK,!pval,0);}

        if (sprefname=="OptBlueChallenge" and pval)
        {SendMessage(OptBlueChallenge,BM_SETCHECK,pval,0);}

        if (sprefname=="EditFailAwayPause")
        {SendMessage(EditFailAwayPause,WM_SETTEXT,0,(LPARAM)prefval);}

        if (sprefname=="EditFailPresentCountdown")
        {SendMessage(EditFailPresentCountdown,WM_SETTEXT,0,(LPARAM)prefval);}

        if (sprefname=="BTargetDevice")
        {
            btargetdevice=prefval;

        }

        if (sprefname=="BTargetName")
        {
            btargetname=prefval;
            LVITEM lvi={};
            lvi.iItem=ListView_GetItemCount(OutList)+2;
            int ni=ListView_InsertItem(OutList,&lvi);
            ListView_SetItemText(OutList,ni,0,(LPSTR)btargetname.c_str());
            ListView_SetItemText(OutList,ni,1,(LPSTR)btargetdevice.c_str());
        }





        if (sprefname=="CurWindow")
        {curwnd=(HWND)pval;}

        if (!mode and sprefname=="CloneList")
        {
            string oldclone;
            stringstream ss(prefval);
            char aval[1000];

            while (ss.getline(aval,500,'*'))
            {
                oldclone=string(aval);
                if (oldclone.length())
                {
                    procset tps={};
                    strncpy(tps.c,oldclone.c_str(),254);
                    hlist.push_back(tps);
                }
            }


        }


    }


    memset(buffer,0,sizeof(buffer));

}


void SavePrefs(int mode)
{
    char buffer[MAX_PATH*6]={};
    string prefs;


    if(!FindWindowEx(mwnd,NULL,"BUTTON","Help"))
    {
        prefs=memprefs;
        prefs.replace(prefs.find("CloneList"),string::npos,"");

        clonelist="";
        for (uint x=0;x<hlist.size();x++)
        {
            clonelist+=string(hlist[x].c)+"*";
        }

        prefs+="CloneList\x1b";
        prefs+=clonelist;
        prefs+="\n";
        prefs+="\xff";

        memprefs=prefs;
        if (mode){return;}

        prefs=xscramble(prefs);
        FILE * fp;
        fp=fopen(preffile.c_str(),"w+");
        if (fp==NULL){return;}
        fputs(prefs.c_str(),fp);
        fclose(fp);

        return;
    }

    prefs+="Salt" "\x1b";
    prefs+=savedsalt+"\n";

    prefs+="Saltedpasshash" "\x1b";
    prefs+=saltedpasshash+"\n";

    prefs+="EditIntervalDuration\x1b";
    SendMessage(EditIntervalDuration,WM_GETTEXT,255,(LPARAM)buffer);
    prefs+=buffer;prefs+="\n";

    prefs+="OptIntervalChallenge\x1b";
    prefs+=ltoa(SendMessage(OptIntervalChallenge,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptUSBChallenge\x1b";
    prefs+=ltoa(SendMessage(OptUSBChallenge,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptUSBShutDown\x1b";
    prefs+=ltoa(SendMessage(OptUSBShutDown,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="EditChallengePrompt\x1b";
    SendMessage(EditChallengePrompt,WM_GETTEXT,999,(LPARAM)buffer);
    string ecp=buffer;
    replace(ecp.begin(),ecp.end(),char(13),char(27));
    replace(ecp.begin(),ecp.end(),char(10),char(255));
    prefs+=ecp;prefs+="\n";

    prefs+="EditFailSeconds\x1b";
    SendMessage(EditFailSeconds,WM_GETTEXT,999,(LPARAM)buffer);
    prefs+=buffer;prefs+="\n";

    prefs+="EditFailKeystrokes\x1b";
    SendMessage(EditFailKeystrokes,WM_GETTEXT,999,(LPARAM)buffer);
    prefs+=buffer;
    prefs+="\n";

    prefs+="EditPauseMinutes\x1b";
    SendMessage(EditPauseMinutes,WM_GETTEXT,999,(LPARAM)buffer);
    prefs+=buffer;prefs+="\n";

    prefs+="OptFailSeconds\x1b";
    prefs+=ltoa(SendMessage(OptFailSeconds,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptFailKeystrokes\x1b";
    prefs+=ltoa(SendMessage(OptFailKeystrokes,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptAudibleBeeps\x1b";
    prefs+=ltoa(SendMessage(OptAudibleBeeps,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptAutoHide\x1b";
    prefs+=ltoa(SendMessage(OptAutoHide,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptAutoStart\x1b";
    prefs+=ltoa(SendMessage(OptAutoStart,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptTestMode\x1b";
    prefs+=ltoa(SendMessage(OptTestMode,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptCreateClones\x1b";
    prefs+=ltoa(SendMessage(OptCreateClones,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptRandLoc\x1b";
    prefs+=ltoa(SendMessage(OptRandLoc,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptSystemCritical\x1b";
    prefs+=ltoa(SendMessage(OptSystemCritical,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptFailAway\x1b";
    prefs+=ltoa(SendMessage(OptFailAway,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptFailPresent\x1b";
    prefs+=ltoa(SendMessage(OptFailPresent,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="OptBlueChallenge\x1b";
    prefs+=ltoa(SendMessage(OptBlueChallenge,BM_GETCHECK,0,0),buffer,10);
    prefs+="\n";

    prefs+="EditFailAwayPause\x1b";
    SendMessage(EditFailAwayPause,WM_GETTEXT,999,(LPARAM)buffer);
    prefs+=buffer;prefs+="\n";

    prefs+="EditFailPresentCountdown\x1b";
    SendMessage(EditFailPresentCountdown,WM_GETTEXT,999,(LPARAM)buffer);
    prefs+=buffer;prefs+="\n";

    prefs+="BTargetDevice\x1b";
    prefs+=btargetdevice;prefs+="\n";

    prefs+="BTargetName\x1b";
    prefs+=btargetname;prefs+="\n";

    prefs+="CurWindow\x1b";
    prefs+=ltoa((long)curwnd,buffer,10);
    prefs+="\n";

    clonelist="";

    for (uint x=0;x<hlist.size();x++)
    {
        clonelist+=string(hlist[x].c)+"*";
    }


    prefs+="CloneList\x1b";
    prefs+=clonelist;
    prefs+="\n";
    prefs+="\xff";

    memprefs=prefs;
    if (mode){return;}

    prefs=xscramble(prefs);
    FILE * fp;
    fp=fopen(preffile.c_str(),"w+");
    if (fp==NULL){return;}
    fputs(prefs.c_str(),fp);
    fclose(fp);

}


BOOL EnablePriv(LPCSTR lpszPriv)
{
    HANDLE hToken;
    LUID luid;
    TOKEN_PRIVILEGES tkprivs;
    ZeroMemory(&tkprivs, sizeof(tkprivs));

    if(!OpenProcessToken(GetCurrentProcess(), (TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY), &hToken))
        return FALSE;

    if(!LookupPrivilegeValue(NULL, lpszPriv, &luid)){
        CloseHandle(hToken); return FALSE;
    }

    tkprivs.PrivilegeCount = 1;
    tkprivs.Privileges[0].Luid = luid;
    tkprivs.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    BOOL bRet = AdjustTokenPrivileges(hToken, FALSE, &tkprivs, sizeof(tkprivs), NULL, NULL);
    CloseHandle(hToken);
    return bRet;
}



inline LRESULT MakeCritical(bool opt=0)
{
    syscrit=0;
    typedef long ( __cdecl *RtlSetProcessIsCritical ) (IN BOOLEAN bNew, OUT BOOLEAN *pbOld, IN BOOLEAN bNeedScb );
    RtlSetProcessIsCritical SetCriticalProcess;

    HMODULE ntdll = LoadLibrary("ntdll.dll");
    if (ntdll != NULL)
    {
        SetCriticalProcess = (RtlSetProcessIsCritical)GetProcAddress((HINSTANCE)ntdll, "RtlSetProcessIsCritical");
        if (SetCriticalProcess and EnablePriv(SE_DEBUG_NAME)){SetCriticalProcess(opt, NULL, FALSE);}
        syscrit=CheckCritical(GetCurrentProcess());
    }
    FreeLibrary(ntdll);
    return 0;

}


ULONG CheckCritical(HANDLE h)
{
    typedef LONG (WINAPI *PROCNTQSIP)(IN HANDLE ProcessHandle,UINT,PVOID,ULONG,PULONG);
    PROCNTQSIP NtQueryInformationProcess;

    ULONG nqip=1;

    HMODULE ntdll=LoadLibrary("ntdll");
    if (ntdll!=NULL)
    {
        NtQueryInformationProcess = (PROCNTQSIP)GetProcAddress((HINSTANCE)ntdll,"NtQueryInformationProcess");
        if (NtQueryInformationProcess){NtQueryInformationProcess(h,29,&nqip,sizeof(ULONG),NULL);}
    }

    FreeLibrary(ntdll);
	return nqip;
}



void RegisterUSB()
{
        DEV_BROADCAST_DEVICEINTERFACE NotificationFilter ={};
        NotificationFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
        NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
        NotificationFilter.dbcc_classguid = {0xA5DCBF10L, 0x6530, 0x11D2, 0x90, 0x1F, 0x00,0xC0, 0x4F, 0xB9, 0x51, 0xED};
        RegisterDeviceNotification(hwnd,&NotificationFilter,DEVICE_NOTIFY_WINDOW_HANDLE);
        WTSRegisterSessionNotification(hwnd,NOTIFY_FOR_ALL_SESSIONS);
}

void toggleactive()
{

    if (timermode==2){return;}
    if (lockfail){return;}
    if (timermode==1){challenge();}
    if (lockfail){return;}
    if (quitstate){return;}

    if (!timermode and !saltedpasshash.length())
    {
        if (autorun)
        {
            MessageBox(hwnd,(LPCSTR)"Auto-run has been canceled because no password has been set.",(LPCSTR)"ShutSentry",MB_OK);
            exit(0);
            return;
        }

        MessageBox(hwnd,(LPCSTR)"Password needed for operation",(LPCSTR)"",MB_OK);
        SetFocus(EditPassword);
        return;
    }


    if (timermode and pausemode){PauseOp(0);}

    if (!timermode)
    {

        if (SendMessage(OptSystemCritical,BM_GETCHECK,0,0) and !alreadyadmin)
        {RestartProgram("?toggleon?admin");return;}

        if(SendMessage(OptSystemCritical,BM_GETCHECK,0,0) and !SendMessage(OptTestMode,BM_GETCHECK,0,0))
        {MakeCritical(true);}

        testmode=SendMessage(OptTestMode,BM_GETCHECK,0,0);
        randloc=SendMessage(OptRandLoc,BM_GETCHECK,0,0);
        guardmode=SendMessage(OptCreateClones,BM_GETCHECK,0,0);

        char buffer[1000];
        GetWindowText(EditIntervalDuration,buffer,sizeof(buffer));
        challint=atol(buffer) * 60000;
        timervalue=challint;
        GetWindowText(EditChallengePrompt,buffer,sizeof(buffer));
        challengeprompt=buffer;
        GetWindowText(EditFailSeconds,buffer,sizeof(buffer));
        shutint=atol(buffer) * 1000;
        origshutint=shutint;
        GetWindowText(EditFailKeystrokes,buffer,sizeof(buffer));
        maxkeystrokes=atol(buffer);
        SavePrefs();

        usbaction=0;
        if(SendMessage(OptUSBChallenge,BM_GETCHECK,0,0)){usbaction=1;}
        if(SendMessage(OptUSBShutDown,BM_GETCHECK,0,0)){usbaction=2;}
        intervalaction=SendMessage(OptIntervalChallenge,BM_GETCHECK,0,0);
        failsecondsaction=SendMessage(OptFailSeconds,BM_GETCHECK,0,0);
        failkeystrokesaction=SendMessage(OptFailKeystrokes,BM_GETCHECK,0,0);
        failaway=SendMessage(OptFailAway,BM_GETCHECK,0,0);
        failpresent=SendMessage(OptFailPresent,BM_GETCHECK,0,0);
        GetWindowText(EditFailAwayPause,buffer,sizeof(buffer));
        failawaypause=atol(buffer) * 60000;
        GetWindowText(EditFailPresentCountdown,buffer,sizeof(buffer));
        failpresentcountdown=atol(buffer) * 1000;
        bluetoothcheck=SendMessage(OptBlueChallenge,BM_GETCHECK,0,0);


        beepnote=SendMessage(OptAudibleBeeps,BM_GETCHECK,0,0);

        chiefid=GetCurrentProcessId();
        cwnd=mwnd;

        if(guardmode and hlist.size()<10){gcheck(1);}


        if (bluetoothcheck)
        {
            int * n = new int(0);
            CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)bcheckthread,n,0,NULL);
        }





    }

    EnumChildWindows(hwnd,&EnumChildProc,timermode);

    if(!timermode and SendMessage(OptAutoHide,BM_GETCHECK,0,0)){ZapWindows();}

    timermode=!timermode;


}


long fcheck()
{
    if (procrole){return 1;}
    if (hlist.size() and quitstate){quitstate=qstart;}

    for (uint x=0;x<hlist.size();x++)
    {
        if (hlist[x].active==1 and !timermode)
        {
            COPYDATASTRUCT cd={};
            cd.dwData=3;
            cd.cbData=strlen(hlist[x].a)+1;
            cd.lpData=(void*)(hlist[x].a);
            DWORD dres=0;
            SendMessageTimeout(hlist[x].w,WM_COPYDATA,(WPARAM)hwnd,(LPARAM)&cd,SMTO_NORMAL,1000,&dres);

            if (dres==11 or !WaitForSingleObject(hlist[x].p,0))
            {
                CloseHandle(hlist[x].p);
                hlist[x].active=0;
            }
        }

        if (hlist[x].active<=0)
        {
            if (dcheck(hlist[x].c) or hlist[x].active==-1){hlist.erase(hlist.begin()+x--);}
        }
    }

    if (hlist.size()){return 0;}

    return 1;
}





void qcheck()
{
    if (quitstate)
    {
        if (quitstate==1){SavePrefs();}
        quitstate++;
        ShowWindow(hwnd,SW_HIDE);
        if (fwnd){SendMessage(fwnd,WM_CLOSE,0,0);}
    }

    if (quitstate>=qnow){quitstate=qnow;quitself();}
}

void WindowClose()
{
    if (timermode==2){return;}
    if (timermode==1){challenge();}

    if (lockfail){return;}

    ShowWindow(hwnd,SW_HIDE);
    timermode=0;
    quitstate=qstart;
}

void CallConsole()
{
    AllocConsole();
    HWND hWndConsole = GetConsoleWindow();
    SetWindowPos(hWndConsole, 0, 0, 0, 0, 0, SWP_NOZORDER);
    FreeConsole();
}


void PauseOp(int mode)
{
    if (timermode!=1){return;}
    if (lockfail){return;}
    static char buffer[255]={'3','0'};

    if (mode)
    {
        if (!pausemode)
        {
            challenge();
            if (lockfail){return;}
            GetWindowText(EditPauseMinutes,buffer,sizeof(buffer));
            pausemode=60000*atol(buffer);
            if (!pausemode){return;}
        }
        SetWindowText(PauseButton,"Unpause");
        SetWindowText(LabelPause0,"Paused");
        InvalidateRect(LabelPause4,0,true);
        SendMessage(LabelPause0,WM_SETFONT,(WPARAM)hf4,true);
        EnableWindow(EditPauseMinutes,false);
    }
    else
    {
        pausemode=0;
        if (!IsWindowVisible(mwnd)){return;}
        EnableWindow(EditPauseMinutes,true);
        SetWindowText(EditPauseMinutes,buffer);
        SetWindowText(PauseButton,"Pause");
        SetWindowText(LabelPause0,"Running");
        SetWindowText(LabelPause4," Current Status:");
        InvalidateRect(LabelPause4,0,true);
        SendMessage(LabelPause0,WM_SETFONT,(WPARAM)hf3,true);
    }

}



BOOL CALLBACK EnumChildProc(HWND hwnd, LPARAM lParam)
{
    char buffer[255];
    GetClassName(hwnd,buffer,sizeof(buffer));
    if (hwnd==StartButton or hwnd==PauseButton or hwnd==EditPauseMinutes or hwnd==HideButton or hwnd==QuitButton or hwnd==HelpButton or !strcmp(buffer,"Static")){return true;}
    else {EnableWindow(hwnd,lParam ? true : false);}

    if (lParam)
    {
        SendMessage(StartButton,WM_SETTEXT,0,(LPARAM)"Start" );
        SetWindowText(LabelPause0,"Inactive");
        SendMessage(LabelPause0,WM_SETFONT,(WPARAM)hf,true);
        InvalidateRect(LabelPause4,0,true);
        InvalidateRect(LabelPause0,0,true);
        ShowWindow(LabelPause1,SW_HIDE);
        ShowWindow(LabelPause2,SW_HIDE);
        ShowWindow(EditPauseMinutes,SW_HIDE);
        ShowWindow(PauseButton,SW_HIDE);
    }
    else
    {
        SendMessage(StartButton,WM_SETTEXT,0,(LPARAM)"Stop" );
        SetWindowText(LabelPause0,"Running");
        SendMessage(LabelPause0,WM_SETFONT,(WPARAM)hf3,true);
        InvalidateRect(LabelPause4,0,true);
        InvalidateRect(LabelPause0,0,true);
        ShowWindow(LabelPause0,SW_SHOW);
        ShowWindow(LabelPause1,SW_SHOW);
        ShowWindow(LabelPause2,SW_SHOW);
        ShowWindow(EditPauseMinutes,SW_SHOW);
        ShowWindow(PauseButton,SW_SHOW);
    }

    return true;
}



string inconame()
{
    vector<string> inames;

    HANDLE hndl = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS | TH32CS_SNAPMODULE, 0);
    if(hndl)
    {
        char un[1000];
        DWORD unl=1000;
        GetUserName(un,&unl);
        PROCESSENTRY32  process = { sizeof(PROCESSENTRY32) };
        Process32First(hndl, &process);
        do
        {
          HANDLE ph=OpenProcess(PROCESS_QUERY_INFORMATION,true,process.th32ProcessID);
          if (!ph){continue;}
          HANDLE hToken;
          OpenProcessToken(ph, TOKEN_READ, &hToken);
          TOKEN_USER tu={};
          DWORD len=0;
          GetTokenInformation(hToken,TokenUser,&tu,sizeof(TOKEN_USER),&len);
          PTOKEN_USER ptu= (PTOKEN_USER) LocalAlloc(LPTR,len);
          GetTokenInformation(hToken,TokenUser,ptu,len,NULL);
          char buffer1[1000]={},buffer2[1000]={};
          SID_NAME_USE snu;
          DWORD dw999=999;
          LookupAccountSid(NULL,ptu->User.Sid,buffer1,&dw999,buffer2,&dw999,&snu);
          LocalFree(ptu);
          CloseHandle(hToken);
          if (strcmp(buffer1,un)){continue;}




          string tester=process.szExeFile;
          if (tester.find(".exe")!=string::npos){inames.push_back(process.szExeFile);}
        } while(Process32Next(hndl, &process));
        CloseHandle(hndl);
    }

        inames.push_back("conhost.exe");
        inames.push_back("taskhost.exe");
        inames.push_back("taskmgr.exe");
        inames.push_back("explorer.exe");
        inames.push_back("dwm.exe");
        inames.push_back("browser_broker.exe");
        inames.push_back("LockApp.exe");
        inames.push_back("sihost.exe");
        inames.push_back("smartscreen.exe");
        inames.push_back("SearchUI.exe");
        inames.push_back("RunTimeBroker.exe");

        string nret=inames[rand() % inames.size()];
        return nret;

}


void gcheck(int reset)
{
    static int sclones=0;

    if (reset){sclones=1;}
    if (procrole or !timermode){return;}
    if (guardmode and sclones and cloneself(3)){sclones--;}
}


void RestartProgram(string s)
{
    string targetname=meprocname;

    if (selflauncher==""){selflauncher=meprocname;}
    s+="?selflauncher*"+selflauncher;

    if (s.find("?deleteme")!=string::npos)
    {
        s.replace(s.find("?deleteme"),9,"?deleteme*"+meprocname);
        targetname=selflauncher;
    }

    string argz=s;

    if (s.find("?admin")!=string::npos)
    {
        SavePrefs();
        SHELLEXECUTEINFO shExInfo = {0};
        shExInfo.cbSize = sizeof(shExInfo);
        shExInfo.fMask = SEE_MASK_FLAG_NO_UI|SEE_MASK_NOCLOSEPROCESS;
        shExInfo.hwnd = 0;
        shExInfo.lpVerb = _T("runas");
        shExInfo.lpParameters = _T(argz.c_str());
        shExInfo.lpFile = _T(targetname.c_str());
        shExInfo.lpDirectory = 0;
        shExInfo.nShow = SW_SHOW;
        shExInfo.hInstApp = 0;
        ShellExecuteEx(&shExInfo);
        DeleteObs();
        exit(0);
    }

    else
    {
        STARTUPINFO si={};;
        si.cb = sizeof(si);
        PROCESS_INFORMATION pi={};
        si.dwFlags=STARTF_FORCEOFFFEEDBACK;
        string comd=targetname+" "+argz;

        CreateProcess(NULL, (char*)comd.c_str(), NULL, NULL, true, 0, NULL, NULL, &si, &pi);
        CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
    }

    Sleep(100);
    ShowWindow(hwnd,SW_HIDE);
    selfdelete=0;
    quitstate=qstart;

}



LRESULT CALLBACK WindowProcedureSB(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
       case WM_GETDLGCODE:
        {return DLGC_WANTALLKEYS;}


        case WM_KEYDOWN:
        case WM_KEYUP:
        {
            if (wParam==VK_RETURN){wParam=VK_SPACE;}
            break;
        }

        default:
             return CallWindowProc((WNDPROC)sbproc,hwnd,message,wParam,lParam);

    }

     return CallWindowProc((WNDPROC)sbproc,hwnd,message,wParam,lParam);
}


void DeleteObs()
{
    DeleteObject(beigebrush);
    DeleteObject(bluebrush);
    DeleteObject(greybrush);
    DeleteObject(dbeigebrush);
    if (procrole){return;}
    DeleteObject(hf2);
    DeleteObject(hf);
    DeleteObject(hf3);
    DeleteObject(hf4);
}



void dobeeps()
{
    for (int x=0;x<3;x++)
    {Beep(800,800);Sleep(800);}
}



void ShowHelp()
{
    if (HelpWindow){ShowWindow(HelpWindow,SW_SHOW);SetFocus(HelpEdit1);HideCaret(HelpEdit1);return;}
    LoadLibrary("Riched20.dll");
    RECT hrec;
    GetWindowRect(hwnd,&hrec);
    HelpWindow = CreateWindowEx (0,szClassName,"Help: ShutSentry Documentation",WS_OVERLAPPED|WS_SYSMENU,hrec.left,hrec.top,hrec.right-hrec.left,hrec.bottom-hrec.top,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    GetClientRect(HelpWindow,&hrec);
    HelpEdit1 = CreateWindowEx(0,RICHEDIT_CLASS,"",ES_MULTILINE | WS_CHILD | WS_TABSTOP | WS_VSCROLL | ES_NOHIDESEL | ES_AUTOVSCROLL| ES_READONLY,
    0 hh,0 vv,hrec.right-hrec.left,hrec.bottom-hrec.top,HelpWindow,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
    ShowWindow(HelpEdit1,SW_SHOW);
    SendMessage(HelpEdit1,EM_SETBKGNDCOLOR,0,RGB(233,236,216));
    SetWindowLong(HelpEdit1, GWL_EXSTYLE, GetWindowLong(HelpEdit1,GWL_EXSTYLE)&~WS_EX_CLIENTEDGE );
    SetWindowLong(HelpEdit1, GWL_STYLE, GetWindowLong(HelpEdit1,GWL_STYLE)&~WS_TABSTOP );
    SendMessage(HelpEdit1,EM_SETEVENTMASK,0,ENM_LINK);
    SendMessage(HelpEdit1, EM_AUTOURLDETECT, true, 0);
    dproc=SetWindowLong(HelpEdit1,GWL_WNDPROC,(long)EditorStaticProc);
    SetWindowLong(HelpWindow,GWL_WNDPROC,(long)WindowProcedureH);
    ShowWindow(HelpWindow,SW_SHOW);

    SendMessage(HelpEdit1,WM_SETFONT,(WPARAM)hf,true);
    SendMessage(HelpEdit1,EM_SETMARGINS,EC_LEFTMARGIN|EC_RIGHTMARGIN,MAKEWORD(50,50));

    SendMessage(HelpEdit1,EM_GETRECT,0,(LPARAM)&hrec);
    hrec.left=20;hrec.top=20;hrec.right-=20;hrec.bottom-=10;
    SendMessage(HelpEdit1,EM_SETRECT,0,(LPARAM)&hrec);
    HRSRC htext1 = FindResource( NULL, "HELPFILE", RT_RCDATA );
    HGLOBAL ghtext1 = LoadResource( NULL, htext1 );
    const char* chtext1 = (char*)LockResource( ghtext1 );

    SendMessage(HelpEdit1,WM_SETTEXT,0,(LPARAM)chtext1);
    SendMessage(HelpEdit1, WM_SETREDRAW, 0, 0);
    CHARFORMAT cfor={};cfor.cbSize=sizeof(CHARFORMAT);cfor.dwMask=CFM_SIZE;cfor.yHeight=12*fontratio*20;
    SendMessage(HelpEdit1,EM_SETCHARFORMAT,SCF_ALL,(LPARAM)&cfor);
    SendMessage(HelpEdit1, WM_SETREDRAW, -1, 0);
    InvalidateRect(HelpEdit1,0,0);

    ShowWindow(HelpWindow,SW_SHOW);
    SetFocus(HelpEdit1);

}


LRESULT CALLBACK EditorStaticProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{


    switch (message)
    {

    case WM_KEYDOWN:
        if (wParam==27)
        {return 0;}
        HideCaret(hwnd);
        break;


    case WM_LBUTTONDOWN:
    case WM_LBUTTONDBLCLK:
    case WM_RBUTTONDOWN:
    case WM_RBUTTONDBLCLK:
    case WM_KEYUP:
    {HideCaret(hwnd);break;}


    default:

        LRESULT res = CallWindowProc((WNDPROC)dproc,hwnd,message,wParam,lParam);
        HideCaret(hwnd);
        return res;
    }

        LRESULT res = CallWindowProc((WNDPROC)dproc,hwnd,message,wParam,lParam);
        HideCaret(hwnd);
        return res;

}


LRESULT CALLBACK WindowProcedureH (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
    case WM_SYSCOMMAND:
        if (wParam==SC_CLOSE)
        {HelpWindow=0;}
        break;


    case WM_CTLCOLORSTATIC:
    {
        SetBkColor((HDC)wParam,RGB(233,236,216));
        return (LRESULT)beigebrush;
    }


    default:
    return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return DefWindowProc (hwnd, message, wParam, lParam);

}



long CheckSus(HANDLE ch)
{
    DWORD chid=GetProcessId(ch);
    HANDLE h = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
     if (h != INVALID_HANDLE_VALUE) {
      THREADENTRY32 te;
      te.dwSize = sizeof(te);
      if (Thread32First(h, &te))
      {
           do
           {
             if (te.dwSize >= FIELD_OFFSET(THREADENTRY32, th32OwnerProcessID) + sizeof(te.th32OwnerProcessID))
             {
               if (te.th32OwnerProcessID==chid)
               {
                    HANDLE newh=OpenThread(THREAD_ALL_ACCESS,false,te.th32ThreadID);
                    DWORD rt=ResumeThread(newh);
                    CloseHandle(newh);
                    if (rt){sdown(21);}
               }
             }
           te.dwSize = sizeof(te);
           } while (Thread32Next(h, &te));
      }
      CloseHandle(h);
     }
     return 0;
}


long CheckSus2(HANDLE ch)
{

    typedef LONG KPRIORITY;

    typedef struct _UNICODE_STRING
    {
      USHORT Length;
      USHORT MaximumLength;
      PWSTR  Buffer;
    } UNICODE_STRING;

    typedef struct _SYSTEM_PROCESS_INFORMATION
    {
        ULONG NextEntryOffset;
        ULONG NumberOfThreads;
        BYTE Reserved1[48];
        UNICODE_STRING ImageName;
        KPRIORITY BasePriority;
        HANDLE UniqueProcessId;
        PVOID Reserved2;
        ULONG HandleCount;
        ULONG SessionId;
        PVOID Reserved3;
        SIZE_T PeakVirtualSize;
        SIZE_T VirtualSize;
        ULONG Reserved4;
        SIZE_T PeakWorkingSetSize;
        SIZE_T WorkingSetSize;
        PVOID Reserved5;
        SIZE_T QuotaPagedPoolUsage;
        PVOID Reserved6;
        SIZE_T QuotaNonPagedPoolUsage;
        SIZE_T PagefileUsage;
        SIZE_T PeakPagefileUsage;
        SIZE_T PrivatePageCount;
        LARGE_INTEGER Reserved7[6];
    } SYSTEM_PROCESS_INFORMATION;


    typedef struct _CLIENT_ID
    {
       HANDLE UniqueProcess;
       HANDLE UniqueThread;
    } CLIENT_ID;


    typedef struct _SYSTEM_THREAD_INFORMATION
    {
        LARGE_INTEGER KernelTime;
        LARGE_INTEGER UserTime;
        LARGE_INTEGER CreateTime;
        ULONG         WaitTime;
        PVOID         StartAddress;
        CLIENT_ID     ClientID;
        LONG          Priority;
        LONG          BasePriority;
        ULONG         ContextSwitches;
        LONG          ThreadState;
        LONG          WaitReason;
    } SYSTEM_THREAD_INFORMATION;


   typedef LONG (WINAPI* ntqsi)(ULONG, PVOID, ULONG, PULONG);

   DWORD chid=GetProcessId(ch);

   HMODULE ntdll=LoadLibrary("ntdll");
   if (ntdll==NULL){return CheckSus(ch);}

   ntqsi NtQuerySystemInformation = (ntqsi)GetProcAddress((HINSTANCE)ntdll,"NtQuerySystemInformation");
   if (!NtQuerySystemInformation){return CheckSus(ch);}

   int sus=0;
   ULONG retlength=0;
   ULONG ilength=1;
   BYTE * bp = new byte[ilength];

   do
   {
        delete[] bp;
        ilength=retlength+1;
        bp = new byte[ilength];
        NtQuerySystemInformation(5,bp,ilength,&retlength);
   } while (retlength>ilength);

    SYSTEM_PROCESS_INFORMATION * spi = (SYSTEM_PROCESS_INFORMATION *)bp;

    int lastentry=0;

    while(!lastentry)
    {
        if((DWORD)(spi->UniqueProcessId)==chid)
        {
            SYSTEM_THREAD_INFORMATION * sti;

            for (uint x=0;x<spi->NumberOfThreads;x++)
            {
                sti = (SYSTEM_THREAD_INFORMATION*)((BYTE*)spi + sizeof(SYSTEM_PROCESS_INFORMATION) + sizeof(SYSTEM_THREAD_INFORMATION)*x);
                if (sti->ThreadState==5 and sti->WaitReason==5){sus=1;break;}
            }
        }
        if (!spi->NextEntryOffset){lastentry=1;}
        spi=(SYSTEM_PROCESS_INFORMATION*)((LPBYTE)spi+spi->NextEntryOffset);
    }

    delete[] bp;
    FreeLibrary(ntdll);
    return sus;
}



string pickfolder()
{

  char selfdir[MAX_PATH*2];
  WIN32_FIND_DATA FileInformation;
  HANDLE hFile;

  GetModuleFileNameA(NULL, selfdir, MAX_PATH);
  strchr(selfdir,'\\')[0]=0;
  strcat(selfdir,"\\*.*");

  int fchoice=0,ftotal=0;

  while (!fchoice or !ftotal)
  {
          hFile = ::FindFirstFile(selfdir, &FileInformation);
          if(hFile != INVALID_HANDLE_VALUE)
          {
            do
            {
                if(FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                {
                    if (FileInformation.cFileName[0]=='.'){continue;}
                    if (FileInformation.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN){continue;}
                    if (FileInformation.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM){continue;}
                    ftotal++;

                    if (ftotal==fchoice)
                    {
                        strrchr(selfdir,'\\')[0]=0;
                        strcat(selfdir,"\\");
                        strcat(selfdir,FileInformation.cFileName);
                        strcat(selfdir,"\\*.*");
                        break;
                    }

                }
            } while(FindNextFile(hFile, &FileInformation) == TRUE);
          }

          if (!fchoice)
          {
              fchoice=rand()%(ftotal+1);
              ftotal=0;

              if (!fchoice)
              {
                  strrchr(selfdir,'\\')[0]=0;
                  fchoice=-1;
                  break;
              }
          }
          else
          {
              ftotal=0;fchoice=0;
          }


  }

    return string(selfdir);
}


void ModifyExe(string e)
{
    ofstream out;
    out.open(e.c_str(), std::ios::app);
    std::string str = randbytes();
    out << str;
    out.close();

    HANDLE hFile = CreateFile(e.c_str(),FILE_WRITE_ATTRIBUTES,FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
    FILETIME lc,la,lw;
    GetFileTime(hFile,&lc,&la,&lw);
    SYSTEMTIME st;
    FileTimeToSystemTime(&lc,&st);
    lc=lw;la=lw;
    SetFileTime(hFile,&lc,&la,&lw);
    CloseHandle(hFile);

}




BOOL CALLBACK EnumChildProc2(HWND hwnd, LPARAM lParam)
{
    static int ncount=0,filled=0;
    ncount++;

    char buffer[255];
    GetClassName(hwnd,buffer,sizeof(buffer));

    WNDCLASSEX wce;

    if (!GetClassInfoEx(NULL,buffer,&wce) and (!filled or !(rand()%30)))
    {
        strncpy(szClassName,buffer,254);
        filled++;
        if (filled>1){return false;}
    }

    return true;
}


BOOL CALLBACK TrashWindows(HWND hwnd, LPARAM lParam)
{
    static long nc=0;
    nc++;

    if (hwnd!=mwnd){DestroyWindow(hwnd);}

    return true;
}




void ZapWindows(int newmode)
{
        static int mode=0;

        if (newmode>-1){mode=newmode;}

        if (!mode)
        {
            SavePrefs(1);
            GetWindowRect(mwnd,&oldrect);

            ShowWindow(mwnd,SW_HIDE);
            Sleep(1000);
            EnumChildWindows(hwnd,TrashWindows,0);
            SetClassLongPtr(mwnd, GCLP_HBRBACKGROUND, (LONG)backbrush);
            SetWindowText(mwnd,"");
            MoveWindow(mwnd,rand() % 1900,rand() % 1000,rand() % 1900,rand() % 1000,false);
            mode=!mode;
            return;
        }

        mode=0;

        GenerateWindows();

        if (saltedpasshash.length()){SetWindowText(EditPassword,"\x0D[Stored]\x0A");}
        SetClassLongPtr(hwnd, GCLP_HBRBACKGROUND, (LONG)beigebrush);

        loadprefs(1);
        EnumChildWindows(mwnd,&EnumChildProc,!timermode);

        SetClassLongPtr(mwnd, GCLP_HBRBACKGROUND, (LONG)beigebrush);
        SetWindowText(mwnd,"ShutSentry");
        MoveWindow(mwnd,oldrect.left,oldrect.top,oldrect.right-oldrect.left,oldrect.bottom-oldrect.top,true);

        PauseOp(pausemode);
        ShowWindow(mwnd,SW_SHOW);

}

void GenerateWindows()
{
        secx=5;secy=15;

        HWND TextLabel0 = CreateWindow("STATIC","Challenge Trigger Settings",WS_TABSTOP | WS_CHILD | WS_VISIBLE,10 lh, 10 lv,305 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf2,true);

        OptIntervalChallenge=CreateWindow("BUTTON","Challenge every",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,10 lh,43 lv,135 hh,20 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptIntervalChallenge,WM_SETFONT,(WPARAM)hf,true);
        SendMessage(OptIntervalChallenge,BM_SETCHECK,BST_CHECKED,0);

        EditIntervalDuration=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","60",WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_RIGHT|ES_NUMBER,150 lh,40 lv,70 hh,25 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(EditIntervalDuration,WM_SETFONT,(WPARAM)hf,true);

        TextLabel0 = CreateWindow("STATIC","minutes",WS_TABSTOP | WS_CHILD | WS_VISIBLE,230 lh, 43 lv,100 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);

        OptAudibleBeeps = CreateWindow("BUTTON","Sound audible warning first",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,30 lh,75 lv,220 hh,20 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptAudibleBeeps,WM_SETFONT,(WPARAM)hf,true);


        OptUSBChallenge=CreateWindow("BUTTON","Challenge on USB device attach/detach",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,10 lh,110 lv,320 hh,30 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptUSBChallenge,WM_SETFONT,(WPARAM)hf,true);

        OptUSBShutDown=CreateWindow("BUTTON","Shutdown on USB device attach/detach",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,10 lh,140 lv,320 hh,30 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptUSBShutDown,WM_SETFONT,(WPARAM)hf,true);

        OptBlueChallenge=CreateWindow("BUTTON","Challenge on bluetooth device departure:",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,10 lh,170 lv,325 hh,30 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptBlueChallenge,WM_SETFONT,(WPARAM)hf,true);


        INITCOMMONCONTROLSEX icex;
        icex.dwICC = ICC_LISTVIEW_CLASSES;
        InitCommonControlsEx(&icex);

        OutList = CreateWindowEx(WS_EX_CLIENTEDGE,WC_LISTVIEW,"",WS_TABSTOP | WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_NOCOLUMNHEADER|LVS_SHOWSELALWAYS|LVS_SINGLESEL,30 lh,200 lv,290 hh,80 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        ListView_SetExtendedListViewStyle(OutList,LVS_EX_FULLROWSELECT);
        SendMessage(OutList,WM_SETFONT,(WPARAM)hf,true);

        LVCOLUMN lvc={};
        lvc.mask = LVCF_WIDTH|LVCF_SUBITEM;lvc.cx=280 hh;lvc.iSubItem=0;
        SendMessage(OutList,LVM_INSERTCOLUMN,0,(LPARAM)&lvc);
        lvc.cx=0 hh;lvc.iSubItem=1;
        SendMessage(OutList,LVM_INSERTCOLUMN,1,(LPARAM)&lvc);

        ButtonBlueChoose = CreateWindow("BUTTON","Change",WS_TABSTOP | WS_CHILD | WS_VISIBLE, 30 lh,285 lv,100 hh,25 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(ButtonBlueChoose,WM_SETFONT,(WPARAM)hf,true);
        ButtonBlueConfirm = CreateWindow("BUTTON","Confirm",WS_TABSTOP | WS_CHILD, 135 lh,285 lv,100 hh,25 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(ButtonBlueConfirm,WM_SETFONT,(WPARAM)hf,true);

        secx=5;secy=345;

        TextLabel0 = CreateWindow("STATIC","Challenge Settings",WS_TABSTOP | WS_CHILD | WS_VISIBLE,10 lh, 10 lv,305 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf2,true);

        TextLabel0 = CreateWindow("STATIC","Challenge Prompt Text:",WS_TABSTOP | WS_CHILD | WS_VISIBLE,10 lh, 43 lv,305 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);

        EditContainer=CreateWindowEx (0,szClassName,NULL,WS_CHILD|WS_VISIBLE,10 lh, 65 lv ,325 hh,65 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);

        EditChallengePrompt=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","Authentication required",WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_MULTILINE|ES_AUTOVSCROLL|ES_WANTRETURN,0,0,315 hh,65 vv,EditContainer,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(EditChallengePrompt,WM_SETFONT,(WPARAM)hf,true);

        OptFailSeconds= CreateWindow("BUTTON","Auto-fail after ",WS_TABSTOP | WS_CHILD | WS_VISIBLE|BS_AUTOCHECKBOX,10 lh, 143 lv,115 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptFailSeconds,WM_SETFONT,(WPARAM)hf,true);
        SendMessage(OptFailSeconds,BM_SETCHECK,BST_CHECKED,0);

        TextLabel0 = CreateWindow("STATIC","seconds",WS_TABSTOP | WS_CHILD | WS_VISIBLE,210 lh, 148 lv,200 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);

        EditFailSeconds=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","30",WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_RIGHT|ES_NUMBER,130 lh,145 lv,70 hh,25 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(EditFailSeconds,WM_SETFONT,(WPARAM)hf,true);

        OptFailKeystrokes = CreateWindow("BUTTON","Auto-fail after ",WS_TABSTOP | WS_CHILD | WS_VISIBLE|BS_AUTOCHECKBOX,10 lh, 183 lv,115 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptFailKeystrokes,WM_SETFONT,(WPARAM)hf,true);

        TextLabel0 = CreateWindow("STATIC","keystrokes",WS_TABSTOP | WS_CHILD | WS_VISIBLE,210 lh, 188 lv,200 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);

        EditFailKeystrokes=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","0",WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_RIGHT|ES_NUMBER,130 lh,185 lv,70 hh,25 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(EditFailKeystrokes,WM_SETFONT,(WPARAM)hf,true);


        secy=385;

        COLORREF tempbackcolor=RGB(175,200,175);
        tempbackcolor=RGB(191,225,251);
        TextLabel0 = CreateWindow("STATIC","",WS_TABSTOP | WS_CHILD | WS_VISIBLE,0 hh, 220 lv,1000 hh,300 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        bcolor[TextLabel0]=tempbackcolor;
        TextLabelGreen=TextLabel0;
        TextLabel0 = CreateWindow("STATIC","On passive failure of timed interval or bluetooth challenge: ",WS_TABSTOP | WS_CHILD | WS_VISIBLE,10 lh, 233 lv,615 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);bcolor[TextLabel0]=tempbackcolor;
        OptFASD=CreateWindow("BUTTON","Shut down computer",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_GROUP, 15 lh,260 lv,235 hh,30 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        TextLabel0=OptFASD;
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);bcolor[TextLabel0]=tempbackcolor;
        SendMessage(TextLabel0,BM_SETCHECK,BST_CHECKED,0);
        OptFailAway=CreateWindow("BUTTON","Lock computer and then shutdown after",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON, 15 lh,290 lv,300 hh,30 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptFailAway,WM_SETFONT,(WPARAM)hf,true);bcolor[OptFailAway]=tempbackcolor;
        EditFailAwayPause=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","0",WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_RIGHT|ES_NUMBER,315 lh,290 lv,70 hh,25 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(EditFailAwayPause,WM_SETFONT,(WPARAM)hf,true);
        TextLabel0 = CreateWindow("STATIC","minutes  (  0 = never  )",WS_TABSTOP | WS_CHILD | WS_VISIBLE,395 lh, 295 lv,240 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);bcolor[TextLabel0]=tempbackcolor;


        secy=355;
        tempbackcolor=RGB(255,205,100);
        tempbackcolor=RGB(255,205,255);
        tempbackcolor=RGB(216,196,216);
        TextLabel0 = CreateWindow("STATIC","",WS_TABSTOP | WS_CHILD | WS_VISIBLE,0 hh, 220 lv,1000 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        bcolor[TextLabel0]=tempbackcolor;
        TextLabel0=CreateWindow("STATIC","Failure Options ",WS_TABSTOP | WS_CHILD | WS_VISIBLE|SS_CENTERIMAGE,10 lh, 223 lv,615 hh,23 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf3,true);bcolor[TextLabel0]=tempbackcolor;


        secy=450;
        tempbackcolor=RGB(191,225,251);
        tempbackcolor=RGB(175,200,175);
        TextLabel0 = CreateWindow("STATIC","",WS_TABSTOP | WS_CHILD | WS_VISIBLE,0 hh, 270 lv,1000 hh,300 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        bcolor[TextLabel0]=tempbackcolor;
        TextLabel0=CreateWindow("STATIC","On all other challenge failures: ",WS_TABSTOP | WS_CHILD | WS_VISIBLE,10 lh, 283 lv,615 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);bcolor[TextLabel0]=tempbackcolor;
        OptFPSD=CreateWindow("BUTTON","Shut down computer",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_GROUP, 15 lh,310 lv,235 hh,30 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        TextLabel0=OptFPSD;
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);bcolor[TextLabel0]=tempbackcolor;
        SendMessage(TextLabel0,BM_SETCHECK,BST_CHECKED,0);
        OptFailPresent=CreateWindow("BUTTON","Lock computer and then shutdown after",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON, 15 lh,340 lv,290 hh,30 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptFailPresent,WM_SETFONT,(WPARAM)hf,true);bcolor[OptFailPresent]=tempbackcolor;
        EditFailPresentCountdown=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","0",WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_RIGHT|ES_NUMBER,315 lh,340 lv,70 hh,25 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(EditFailPresentCountdown,WM_SETFONT,(WPARAM)hf,true);
        TextLabel0 = CreateWindow("STATIC","seconds  (  0 = never  )",WS_TABSTOP | WS_CHILD | WS_VISIBLE,395 lh, 345 lv,200 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);bcolor[TextLabel0]=tempbackcolor;





        CreateWindow("STATIC","",WS_CHILD | WS_VISIBLE| SS_BLACKRECT,350 hh, 40 vv,2 hh,500 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        CreateWindow("STATIC","",WS_CHILD | WS_VISIBLE| SS_BLACKRECT,620 hh, 40 vv,2 hh,500 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);

        EditPassword=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","\x0D[Needed]\x0A",WS_CHILD | WS_TABSTOP|WS_VISIBLE|ES_CENTER|ES_AUTOHSCROLL,380 hh,80 vv,210 hh,25 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(EditPassword,WM_SETFONT,(WPARAM)hf,true);
        pbproc=SetWindowLong(EditPassword, GWL_WNDPROC, (LONG)WindowProcedurePB);


        StartButton = CreateWindow("BUTTON","Start",WS_TABSTOP | WS_CHILD | WS_VISIBLE, 380 hh,190 vv,210 hh,40 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(StartButton,WM_SETFONT,(WPARAM)hf3,true);
        sbproc=SetWindowLong(StartButton, GWL_WNDPROC, (LONG)WindowProcedureSB);

        QuitButton = CreateWindow("BUTTON","Quit",WS_TABSTOP | WS_CHILD | WS_VISIBLE, 660 hh,500 vv,210 hh,40 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(QuitButton,WM_SETFONT,(WPARAM)hf,true);

        LabelPause4 = CreateWindow("STATIC"," Current Status:",WS_TABSTOP | WS_CHILD |WS_VISIBLE|SS_CENTERIMAGE,380 hh, 240 vv,120 hh,25 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(LabelPause4,WM_SETFONT,(WPARAM)hf,true);

        LabelPause0 = CreateWindow("STATIC","Inactive",WS_TABSTOP | WS_CHILD |WS_VISIBLE|SS_CENTERIMAGE,500 hh, 240 vv,90 hh,25 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(LabelPause0,WM_SETFONT,(WPARAM)hf,true);

        PauseButton = CreateWindow("BUTTON","Pause",WS_TABSTOP | WS_CHILD, 380 hh,300 vv,210 hh,30 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(PauseButton,WM_SETFONT,(WPARAM)hf,true);

        LabelPause1 = CreateWindow("STATIC","Pause time:",WS_TABSTOP | WS_CHILD ,380 hh, 345 vv,100 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(LabelPause1,WM_SETFONT,(WPARAM)hf,true);

        EditPauseMinutes=CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT","30",WS_CHILD | WS_TABSTOP|ES_RIGHT|ES_NUMBER,470 hh,340 vv,50 hh,25 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(EditPauseMinutes,WM_SETFONT,(WPARAM)hf,true);

        LabelPause2= CreateWindow("STATIC","minutes",WS_TABSTOP | WS_CHILD ,525 hh, 345 vv,70 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(LabelPause2,WM_SETFONT,(WPARAM)hf,true);

        TextLabel0 = CreateWindow("STATIC","Password:",WS_TABSTOP | WS_CHILD|WS_VISIBLE ,380 hh, 50 vv,150 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf,true);


        secx=650;secy=15;

        HideButton= CreateWindow("BUTTON","Hide",WS_TABSTOP | WS_CHILD | WS_VISIBLE,380 hh,450 vv,210 hh,40 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(HideButton,WM_SETFONT,(WPARAM)hf,true);

        TextLabel0 = CreateWindow("STATIC","Miscellaneous Settings",WS_TABSTOP | WS_CHILD | WS_VISIBLE,10 lh, 10 lv,190 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf2,true);

        OptAutoHide = CreateWindow("BUTTON","Auto-hide when running",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,10 lh,50 lv,200 hh,20 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptAutoHide,WM_SETFONT,(WPARAM)hf,true);

        OptAutoStart = CreateWindow("BUTTON","Auto-run at logon",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,10 lh,80 lv,200 hh,20 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptAutoStart,WM_SETFONT,(WPARAM)hf,true);

        TextLabel0 = CreateWindow("STATIC","Countermeasure Settings",WS_TABSTOP | WS_CHILD | WS_VISIBLE,10 lh, 160 lv,200 hh,30 vv,hwnd, NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(TextLabel0,WM_SETFONT,(WPARAM)hf2,true);

        OptRandLoc=CreateWindow("BUTTON","(use random locations)",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,30 lh,260 lv,250 hh,20 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptRandLoc,WM_SETFONT,(WPARAM)hf,true);

        OptTestMode = CreateWindow("BUTTON","Test Mode Only",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,10 lh,110 lv,200 hh,20 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptTestMode,WM_SETFONT,(WPARAM)hf,true);
        SendMessage(OptTestMode,BM_SETCHECK,BST_CHECKED,0);

        OptSystemCritical = CreateWindow("BUTTON","Set system critical",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,10 lh,200 lv,200 hh,20 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptSystemCritical,WM_SETFONT,(WPARAM)hf,true);

        OptCreateClones = CreateWindow("BUTTON","Create sentry clones",WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,10 lh,230 lv,200 hh,20 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(OptCreateClones,WM_SETFONT,(WPARAM)hf,true);

        HelpButton= CreateWindow("BUTTON","Help",WS_TABSTOP | WS_CHILD | WS_VISIBLE,380 hh,500 vv,210 hh,40 vv,hwnd,NULL,(HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),NULL);
        SendMessage(HelpButton,WM_SETFONT,(WPARAM)hf,true);

}



void checkexinstance()
{
    if (curwnd and IsWindow(curwnd) and curwnd!=mwnd)
    {
        COPYDATASTRUCT cds={};
        cds.dwData=33;
        cds.cbData=meprocname.length()+1;
        cds.lpData=(void*)meprocname.c_str();
        DWORD dres=0,tries=60;


        while (!SendMessageTimeout(curwnd,WM_COPYDATA,0,(LPARAM)&cds,SMTO_NORMAL,500,&dres) and tries--){Sleep(500);}
        if (dres==33)
        {
            exit(0);
        }
    }

    curwnd=mwnd;


    HANDLE hFile;
    WIN32_FIND_DATA FileInformation;

    string f=selfdir;
    f+="\\clone*.ini";

    hFile = ::FindFirstFile(f.c_str(), &FileInformation);

    if(hFile != INVALID_HANDLE_VALUE and !(FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
    {
        do
        {
                string oldfile=FileInformation.cFileName;
                if (selfdir+"\\"+oldfile==preffile){continue;}
                fstream fs;
                oldfile=selfdir+"\\"+oldfile;
                fs.open(oldfile.c_str(),ios_base::in);

                if(oldfile.substr(selfdir.length()+1,5)=="clone")
                {
                    char aval[1000];
                    while (fs.getline(aval,500,'\n'))
                    {
                        string oldclone=string(aval);
                        if (oldclone.length())
                        {
                            procset tps={};
                            strncpy(tps.c,oldclone.c_str(),254);
                            hlist.push_back(tps);
                        }
                    }
                    fs.close();
                    DeleteFile(oldfile.c_str());
                    continue;
                }

        } while(FindNextFile(hFile, &FileInformation) == TRUE);

        FindClose(hFile);


    }

}


void CheckArgs(LPSTR a)
{

    string arg,subarg;
    stringstream ss(a);
    char aval[255];

    while (ss.getline(aval,200,'?'))
    {
        arg=string(aval);
        subarg="";
        if (arg.find("*")!=string::npos)
        {
            subarg=arg.substr(arg.find("*")+1);
            arg=arg.substr(0,arg.find("*")-0);
        }

        if (arg=="deleteme")
        {
            int tries=60;
            while (!dcheck(subarg) and tries--){Sleep(500);}
            exit(0);
        }

        if (arg=="selfdelete"){selfdelete=1;}
        if (arg=="selflauncher"){selflauncher=subarg;}
        if (arg=="admin"){alreadyadmin=1;}
        if (arg=="toggleon"){toggleon=1;}
        if (arg=="autorun"){autorun=1;}
        if (arg=="randloc"){randloc=1;}
        if (arg=="procrole"){procrole=atol(subarg.c_str());}
        if (arg=="chiefid"){chiefid=atol(subarg.c_str());}
        if (arg=="chiefwindow"){cwnd=(HWND)atol(subarg.c_str());}
        if (arg=="syscrit"){syscrit=1;}
        if (arg=="testmode"){testmode=1;}
        if (arg=="chiefdir"){chiefdir=subarg;}

    }

    return;

}



void spitclone()
{
    string fname=chiefdir+"\\"+"clone"+randname()+".ini";
    fstream fs(fname.c_str(),fs.out);
    fs << meprocname <<endl;
    fs.close();
}




string xscramble(string s)
{
    char xorer[33];
    for (uint x=0;x<sizeof(xorer);x++)
    {xorer[x]=rand();}

    char xor64[64]={};
    Base64Encode(xorer,xor64,sizeof(xorer));

    int psize=s.size();
    for (int i = 0; i < psize; i++)
    {s[i] ^= xorer[i % sizeof(xorer)];}

    char * b64buff = new char[psize*2];
    memset(b64buff,0,psize*2);
    Base64Encode((char*)s.c_str(),b64buff,psize);
    string rstring=string(xor64)+string(b64buff);
    delete[] b64buff;
    return rstring;



}


string xunscramble(string s)
{

    char xorer[33]={};
    char xor64[64]={};

    strncpy(xor64,s.c_str(),44);
    long xsize=Base64Decode(xor64,xorer);

    long ft=strlen(&s[44])+1;
    char * longbuffer = new char[ft];
    char * shortbuffer= new char[ft];
    memset(longbuffer,0,ft);
    memset(shortbuffer,0,ft);

    strncpy(longbuffer,&s[44],ft);
    long dsize=Base64Decode(longbuffer,shortbuffer);

    for (int i = 0; i < dsize; i++)
    {shortbuffer[i] ^= xorer[i % xsize];}

    string rstring=string(shortbuffer);
    delete[] longbuffer;
    delete[] shortbuffer;
    return rstring;




}





void LookArgs()
{
    string shargs=meprocname;
    int n=shargs.rfind('.');
    shargs=shargs.substr(0,n);
    shargs+="args.dat";
    FILE * fargs = fopen(shargs.c_str(),"r");
    if (fargs==NULL){return;}
    fseek(fargs,0,SEEK_END);
    long ft=ftell(fargs)+1;
    fseek(fargs,0,SEEK_SET);
    char * argbuffer = new char[ft];
    memset(argbuffer,0,ft);
    fgets(argbuffer,ft,fargs);
    fclose(fargs);
    CheckArgs(argbuffer);
    dcheck(shargs);
    delete[] argbuffer;
}


void LockComp()
{
    int (__stdcall * LockWorkStation)();
    HMODULE u32=LoadLibrary("USER32.DLL");

    if (u32!=NULL)
    {
        LockWorkStation = (int (__stdcall *)())GetProcAddress(u32, "LockWorkStation");
        if (LockWorkStation!=NULL){LockWorkStation();}
    }

    FreeLibrary(u32);


}



void bcheckthread (LPVOID n)
{
    int nn = *(int*)n;
    int * dn=(int*)n;
    delete dn;

    vector<BLUETOOTH_DEVICE_INFO> blist;

    BLUETOOTH_DEVICE_SEARCH_PARAMS bdsp;
    BLUETOOTH_DEVICE_INFO bdi;
    HBLUETOOTH_DEVICE_FIND hbf;

    ZeroMemory(&bdsp, sizeof(BLUETOOTH_DEVICE_SEARCH_PARAMS));

    bdsp.dwSize = sizeof(BLUETOOTH_DEVICE_SEARCH_PARAMS);
    bdsp.fReturnAuthenticated = nn ? TRUE : FALSE;
    bdsp.fReturnRemembered = nn ? TRUE : FALSE;
    bdsp.fReturnUnknown = TRUE;
    bdsp.fReturnConnected = TRUE;
    bdsp.fIssueInquiry = TRUE;
    bdsp.cTimeoutMultiplier = 6;
    bdsp.hRadio = NULL;

    bdi.dwSize = sizeof(bdi);


    if (nn)
    {
        SetWindowText(ButtonBlueChoose,"Wait");
        EnableWindow(ButtonBlueChoose,false);
        ListView_DeleteAllItems(OutList);
        LVITEM lvi={};
        lvi.iItem=ListView_GetItemCount(OutList)+2;
        int ni=ListView_InsertItem(OutList,&lvi);
        ListView_SetItemText(OutList,ni,0,(LPSTR)"Checking...");
    }


    do
    {
        blist.clear();
        hbf = BluetoothFindFirstDevice(&bdsp, &bdi);
        if (hbf)
        {
            do
            {
                blist.push_back(bdi);
            } while (BluetoothFindNextDevice(hbf, &bdi));

            BluetoothFindDeviceClose(hbf);
        }

        if (nn){ListView_DeleteAllItems(OutList);}

        int found=0;

        for (uint x=0;x<blist.size();x++)
        {
            wstring ws = blist[x].szName;
            string s2=string(ws.begin(), ws.end());
            char buffer[1000]={};
            sprintf(buffer,"%02X:%02X:%02X:%02X:%02X:%02X",blist[x].Address.rgBytes[5],blist[x].Address.rgBytes[4],blist[x].Address.rgBytes[3],blist[x].Address.rgBytes[2],blist[x].Address.rgBytes[1],blist[x].Address.rgBytes[0]);
            string s3=string(buffer);

            if (nn and s2.length())
            {
                LVITEM lvi={};
                lvi.iItem=ListView_GetItemCount(OutList)+2;

                int ni=ListView_InsertItem(OutList,&lvi);
                ListView_SetItemText(OutList,ni,0,(LPSTR)s2.c_str());
                ListView_SetItemText(OutList,ni,1,(LPSTR)s3.c_str());

            }

            printf("%02X:%02X:%02X:%02X:%02X:%02X     %s   \n", blist[x].Address.rgBytes[5],blist[x].Address.rgBytes[4],blist[x].Address.rgBytes[3],blist[x].Address.rgBytes[2],blist[x].Address.rgBytes[1],blist[x].Address.rgBytes[0],s2.c_str());
            if (s3==btargetdevice){found=1;}

        }

        if (WaitForSingleObject(mtx,10000)==WAIT_OBJECT_0){bfound=found;}
        ReleaseMutex(mtx);


        if (nn)
        {
            SetWindowText(ButtonBlueChoose,"Cancel");
            EnableWindow(ButtonBlueChoose,true);
            ShowWindow(ButtonBlueConfirm,SW_SHOW);
            return;
        }




        Sleep(2000);
    } while (1);


    return;


}


void bcheck()
{
    static long blast=0,bseen=0;

    if (procrole){return;}
    if (!bluetoothcheck){return;}
    if (timermode!=1){return;}
    if (pausemode){return;}

    if (WaitForSingleObject(mtx,10000)==WAIT_OBJECT_0)
    {
        blast=(blast+timerinterval)*!bfound;
        if (bfound and !bseen){bseen=1;}
    }
    ReleaseMutex(mtx);

    if (blast>=10000 and bseen)
    {
        bseen=0;
        challenge(1);
    }


}
