#!/bin/bash

# 
#  ShutSentry 2.8-6
#
#  Copyright (C) 2020, the ShutSentry development team
#
#  All rights reserved.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

progver=2.8-6


function zmess { :; }
function zxmess { :; }
function zexit
{ 
	[[ "$0" == 1 ]] && ext=1 || ext=0
	exit $ext
}


##

function bluefail
{	
	declare -i lnsb=0
	lnsb=$lna+15
	if ((uclear==1)); then lnsb=2; clear; fi
	printf "\e[$lnsb;0f\e[40m\e[K\n\e[K\n\e[K"
	printf "\e[$lnsb;0f\e[1m\e[97m\e[42m**Bluetooth Issue**\n\e[40;97m"

	if [[ "$1" == "1" ]];then
		echo "Bluetooth functionality requires the package 'bluez-utils' which does not appear to be installed or working properly. Please try installing from your distribution's repository. Hit any key to go back." | fold -s
	elif [[ "$1" == "2" ]]; then echo "The bluetooth system service does not appear to be running, which prevents bluetooth functionality from working. Hit any key to go back." | fold -s
	elif [[ "$1" == "3" ]]; then echo -e "No bluetooth adapter/controller appears to be available, which prevents bluetooth functionality from working. Hit any key to go back." | fold -s

	fi

	read -n1 bbb
	printf "\e[$lnsb;0f\e[40m\e[K\n\e[K\n\e[K\e[K\n\e[K"
	if ((uclear==1)); then clear; iisettings "uhd";imenu 2;messi "inactive";fi
}


function blueselect
{	

	blueres=$(timeout 0.05 bluetoothctl --help 2>/dev/null)	
	if [ $? != 0 ];then 
		if [[ "$blueres" != *"ver"* ]];then bluefail "1"  #bluez not installed
		else bluefail "2";fi  #bluetooth service not running
		bfail=1
	else
		
		if [[ "$(timeout 0.05 bluetoothctl list)" == "" ]]; then bluefail "3";bfail=1;fi #no controller available
		
	fi

	if ((bfail==1));then return;fi
	ww=$(tput cols) 
	clear
	pdevs=''
	pdevs=$(timeout 3 bluetoothctl devices Paired)
	pdeva=0;pdevaray=()	
	printf "\n\e[39mBelow is a list of already-paired bluetooth devices:\n\n"
	while IFS= read -r line
	do
		IFS=' ' read -ra array <<<"$line"
		devname=${line#*${array[1]}}
		pdeva=$pdeva+1
		pdevarray[$pdeva]="${array[1]}"
		printf "\e[39m"
		seld=''
		if [[ "$bluedevice" == *"${array[1]}"* ]]; then printf "\e[92m"; seld='(Selected)'; fi
		printf "$pdeva) %-30s  (%s)  %s\n" "$devname"  "${array[1]}" "$seld"
		printf "\e[39m"
	done <<< "$pdevs"

	
	echo -e "\nEnter device # (1-$pdeva) to select. Zero for none. Blank for no change:"
	tput cnorm; stty echo
	read blueval
	if [[ ${#blueval} >0 ]]; then
		bluedevice="${pdevarray[$blueval]}"
	fi

	clear
	if ((bluetrig != 1 ));then tput civis;stty -echo;iisettings "uhd";imenu 2;messi "inactive";fi
}



function helplist
{
	echo ""
	echo -e "ShutSentry is a virtual dead-man-switch that protects computers from unauthorized physical access, by presenting an on-screen challenge prompt at regular time intervals. If the correct password is not entered, the system is locked and then optionally shut down after a specified delay. Challenges can also be triggered by USB and Bluetooth device connection and disconnection events.\n\n"
	echo -e " Usage:  shutsentry  [--help] [--set] [--interface] [--silent] [--quit] [--pause]"
	echo ""
	printf " --interface\n"; 
	printf "\t\t\t\e[AStart the program with an interactive interface (default option)\n\n"
	echo -e " --silent"; 
	printf "\t\t\t\e[AStart the program silently in background and enter active mode\n\n" 
	echo -e " --quit"
	printf "\t\t\t\e[AQuit any instance of the program if already running\n\n" 
	echo -e " --pause N"
	printf "\t\t\t\e[APut program in pause mode for N seconds if already running\n\n"
	echo -e " --help"
	printf "\t\t\t\e[AShow this help text\n"
	echo ""
	echo -e " --set"
	printf "\t\t\t\e[ADisplay current program settings, or combine with any of:\n"
	echo -e ""
	echo -e "\t\t\tpinterval=PINTERVAL"
	echo -e "\t\t\t (((Regular challenge interval period)))"
	echo -e "\t\t\t\tSet periodic time interval at which the program will regularly challenge for authentication. Value can be specified either in seconds or using timer notation, i.e. 2h30m15s"
	echo ""
	echo -e "\t\t\tusbcheck=yes/no"
	echo -e "\t\t\t (((Challenge on USB device changes)))"
	echo -e "\t\t\t\tSet USB device checks on or off. If switched on, the program will challenge for authentication any time a USB device is either attached to or detached from the system."
	echo ""
	echo -e "\t\t\tcduration=CDURATION"
	echo -e "\t\t\t (((Challenge window duration)))"
	echo -e "\t\t\t\tSet the duration that the challenge prompt is displayed on screen waiting for authentication, before it times out and automatic failure is incurred, causing the screen to be locked."
	echo ""
	echo -e "\t\t\tpcounter=PCOUNTER"
	echo -e "\t\t\t (((Passive failure shutdown counter)))"
	echo -e "\t\t\t\tSet the countdown time before a system shutdown occurs after a passive failure is logged and the screen is locked. A passive failure is defined as a periodic interval challenge or bluetooth challenge that times out due to non-response. (See cduration above and bluecheck below). A negative numeric value equates to 'never'."
	echo ""
	echo -e "\t\t\tacounter=ACOUNTER"
	echo -e "\t\t\t (((Active failure shutdown counter)))"
	echo -e "\t\t\t\tSet the countdown time before a system shutdown occurs after an active failure is logged and the session is locked. An active failure is defined as any type of failure other than a passive failure. (See above). A negative numeric value equates to 'never'."
	echo ""
	echo -e "\t\t\topmode=test/full"
	echo -e "\t\t\t (((Program core operational mode)))"
	echo -e "\t\t\t\tSet the program's operating mode. 'Full' will result in actual screen locks and system shutdowns when triggered, while 'test' will produce only simulated locks and shutdowns. Useful for familiarizing oneself with the program's functions."
	echo ""
	echo -e "\t\t\tguard=yes/no"
	echo -e "\t\t\t (((Guard main program process)))"
	echo -e "\t\t\t\tSet additional processes to monitor the main program process and automatically trigger a system shutdown if it is abnormally terminated."
	echo ""
	echo -e "\t\t\tpassword"
	echo -e "\t\t\t (((Password for authentication)))"
	echo -e "\t\t\t\tSet or change the authentication password required to satisfy the challenge prompt. The default password if not changed is: password"
	echo ""
	echo -e "\t\t\tprompt=PROMPT"
	echo -e "\t\t\t (((Challenge prompt text displayed)))"
	echo -e "\t\t\t\tSet or change the text displayed during the challenge prompt."
	echo ""
	echo -e "\t\t\ttcmd=TCMD"
	echo -e "\t\t\t (((Terminal execution command)))"
	echo -e "\t\t\t\tSet a custom command for launching a terminal emulator and executing a subsequent command within it. If none of the terminal programs natively supported by ShutSentry are present on the system, this option can be used to specify an available one."
	echo ""
	echo -e "\t\t\tlcmd=LCMD"
	echo -e "\t\t\t (((Screen lock command)))"
	echo -e "\t\t\t\tSet a custom command for locking the screen and the current login session. If none of the screen locking programs natively supported by ShutSentry are present on the system, this option can be used to specify an available one."
	echo ""
	echo -e "\t\t\tzcmd=ZCMD"
	echo -e "\t\t\t (((System shutdown command)))"
	echo -e "\t\t\t\tSet a custom command for shutting down the system. This option can be used to specify additional or alternate instructions to be carried out when a shutdown is triggered."
	echo ""
	echo -e "\t\t\tbluecheck=no/any/specific"
	echo -e "\t\t\t (((Challenge on bluetooth dis/connect)))"
	echo -e "\t\t\t\tSet Bluetooth device checking mode. If set to Any, the program will challenge for authentication any time a bluetooth device is connected or disconnected from the system. If set to Specific, challenges will only occur when the selected bluetooth device is connected or disconnected."
	echo ""
	echo -e "\t\t\tbluedevice"
	echo -e "\t\t\t (((Bluetooth device selection)))"
	echo -e "\t\t\t\tSelect bluetooth device to monitor for disconnections. See bluecheck setting above for more information."
	printf "\n\n\n\t\t\t(END OF HELP)"
	printf "\n\n\n.\n\n"

}

function sanityterm
{
	if [ ! -z "$1" ] && [[ "$tcmd" != "auto"* ]]; then 
		$tcmd "$1" "$2" "$3" &>/dev/null &
		return 1
	fi

	if [ ! -z "$1" ] && [[ "$tcmd" = "auto"* ]]; then 
		$tcom "$1" "$2" "$3" &>/dev/null &
		return 1
	fi
	ter=0
	if xterm -help&>/dev/null;then ter=1;tcom="xterm -e";fi
	if gnome-terminal --help&>/dev/null;then ter=2;tcom="gnome-terminal --window --app-id shut.sen.try --";lcom="gnome-screensaver-command -l";fi
	if konsole --help&>/dev/null;then ter=4;tcom="konsole -p TerminalColumns=100 -p TerminalRows=30 -geometry +50+50 --separate -e";lcom="loginctl lock-session";fi
	if xfce4-terminal --help&>/dev/null;then ter=3;tcom="xfce4-terminal --disable-server -x";lcom="xfce4-screensaver-command --lock";fi
	if [[ $tcmd != "auto"* ]];then ter=5;fi

	if [ -z "$oldtcmd" ];then oldtcmd="$tcom";fi
	if [ -z "$oldlcmd" ];then oldlcmd="$lcom";fi

	if ((ter==0));then echo -e "\nNOTE: None of the the terminal programs natively supported by ShutSentry appear to be installed on this system. However, you can specify a custom terminal execution command to use via the program option:\n\nshutsentry --set tcmd your-terminal-command\n\nFor example:\n\nshutsentry --set tcmd \"xterm -e\"\n\nFor more information use:\nshutsentry --help\n";zexit;fi
}


function ppprompt
{
	((cduration-=1))
	tput civis;stty -echo;
	printf "$prompt\n"
	echo "promptprocess $$" >&$fdp

	idur="$2"
	if ((idur==0));then read -s -t "$cduration" tval
	else read -s tval;fi
	status=$?
	
	if [ -z "$tval" ];then tval="\n";fi
	if (( ! status ));then tval="~~~$tval~~~"; echo $tval >&$fdp;fi

	exec {fdp}>&-; exec {fdp}<&-
}


function lockfail
{
	tput civis;stty -echo;
	printf "\nNOTE: ShutSentry attempted to lock the screen but failed to do so. Please set a custom screen locking command by using:\n\nshutsentry --set lcmd \"screen locking command\"\n\nFor example:\n\nshutsentry --set lcmd \"gnome-screensaver-command --lock\"\n\nFor more information use:\nshutsentry --help\n\n*** Hit any key to exit ***"
	read -s var
	zexit
}



function htime
{
	if [[ "$1" = "" ]];then echo "";return 1;fi
	declare -i ttime=0
	nil=""
	ttime="$1" 
	sttime=$nil
	hours=$((ttime / 3600)) minutes=$(((ttime  % 3600)  / 60)) seconds=$((ttime % 60))
	sttime=$([ $hours -gt 0 ] && printf "%dh" "$hours" || echo -e $nil )
	sttime+=$([ $minutes -gt 0 ] && printf "%02dm" "$minutes" || echo -e $nil )
	sttime+=$([ $seconds -gt 0 ] && printf "%02ds" "$seconds" || echo -e $nil )
	if ((hours==0)) && ((minutes==0)) && ((seconds<=9)); then sttime=""$seconds"s";fi
	if ((hours==0)) && ((minutes>0)); then sttime=""$minutes"m"; sttime+="$( [ $seconds -gt 0 ] && printf "%02ds" "$seconds" )";fi
	if ((hours>0)); then sttime=""$hours"h"; sttime+="$( ( [ $minutes -gt 0 ]  ) && printf "%02dm" "$minutes" )"
	sttime+="$( [ $seconds -gt 0 ] && printf "%02ds" "$seconds" )";fi
	echo "$sttime"
}

function messi
{
	declare -i lnl=$(($lna-4)) lnc=$(($lna-5)) lnh=$(($lna-6))
	lm0=$lnh lm1=$lnc lm2=$lnc lm3=$lnc
	printf "\e[0m\e[40m"
	((opmode==1)) && pfx="" || pfx="(Simulated) "
	declare -i stand=0 m1=0 m2=0 m3=0 m4=0
	coli=0f



	case "$1" in
		"active")
			imode=1;quiti=0;lstatus=1;imenu 1 
			printf "\e[$lnh;$coli\e[0m\e[40m\e[K\e[96m Active" 
			((pinterval > 0)) && printf "\e[$lnc;$coli\e[0m\e[97m\e[40m %-35s" "Interval challenge in:" 
			((pinterval<=0 && usbcheck==1 && bluecheck==0)) && printf "\e[$lnc;0f\e[0m\e[97m\e[40m %-35s" "Monitoring for USB changes... " 
			((pinterval<=0 && usbcheck==1 && bluecheck==1)) && printf "\e[$lnc;0f\e[0m\e[97m\e[40m %-35s" "Monitoring for USB and Bluetooth changes... " 
			((pinterval<=0 && usbcheck==0 && bluecheck==1)) && printf "\e[$lnc;0f\e[0m\e[97m\e[40m %-35s" "Monitoring for Bluetooth changes... " 


			((pinterval<=0 && usbcheck==0 && bluecheck==0)) && printf "\e[$lnc;0f\e[0m\e[97m\e[40m %-35s" "Nothing to check... " 
			;;
		"ctr") 
			((imode!=0)) && printf "\e[$lnl;2f\e[40m\e[97m\e[1m%-20s " "$(htime $2) ";;
		"initializing")
			printf "\e[$lnh;0f\e[93m\e[40m %-25s \n\e[K\n\e[K" "Initializing..." ;;
		"inactive")
			lstatus=0
			printf "\e[$lnh;$coli\e[93m\e[40m %-25s" "Inactive"
			printf "\e[$lnc;$coli\e[95m\e[40m %-55s \n\e[K" "Waiting for start command" ;;
		"starting")
			printf "\e[$lnh;$coli\e[96m\e[40m %-25s \n\e[K" "Starting..." ;;

		"challint")
			printf "\e[$lnh;$coli\e[96m\e[40m %-25s" "Interval Challenge"
			m1=1 ;;
		"paused")
			lstatus=2;imenu 3
			printf "\e[4;0f\e[96m\e[40m %-25s" "Paused"
			printf "\e[5;0f\e[95m\e[40m %-35s" "Resuming in:" ;;
		"pausereq")
			printf "\e[4;0f\e[96m\e[40m %-25s" "Pause request challenge"
			m1=1 ;;
		"stopreq")
			printf "\e[4;0f\e[96m\e[40m\e[K Stop request challenge"
			m1=1 ;;
		"grace")
			lstatus=3
			 printf "\e[4;0f\e[96m\e[40m %-25s" "Passive failure"
			 [[ $pcounter -ge 1 ]] && ((m2=1)) 
			 [[ $pcounter -le 0 ]] && ((m3=1)) ;;
		"chafail")
			 printf "\e[4;0f\e[96m\e[40m %-25s" "Active failure"
			if [[ $acounter -ge 1 ]];then ((m2=1));fi 
			if [[ $acounter -le 0 ]];then  ((m3=1));fi 
			;;
		"sunlocked")
			 printf "\e[$lnh;0f\e[96m\e[40m %-25s" "Screen unlock challenge"
			 if [[ $acounter -ge 1 ]];then ((m2=1));fi 
			 if [[ $acounter -le 0 ]];then  ((m3=1));fi 
			 ;;
		"usbc")
			 printf "\e[$lnh;0f\e[96m\e[40m %-25s" "USB device challenge"
			 m1=1
			 ;;
		"bluet")
			 printf "\e[$lnh;0f\e[96m\e[40m %-25s" "Bluetooth challenge"
			 m1=1
			 ;;

	esac

	((m1)) && printf "\e[$lm1;0f\e[92m\e[40m $pfx%-25s" "Screen lock in:"
	((m2)) && printf "\e[$lm2;0f\e[91;1m\e[40m $pfx%-25s" "System shutdown in:"
	((m3)) && printf "\e[$lm3;0f\e[95m\e[40m $pfx%-25s" "Screen lock engaged"

}


function imess
{
	zmess "$@"
	if [ "$1" != "ctr" ];then lastmess="$1";fi
	if [ "$ipid" != 0 ]; then echo "$1" "$2" "$3" >&$fdi;fi
	if [ "$ipid" != 0 ] && [ "$1" != "ctr" ] && [ "$1" != "iquit" ] && ((ctr>=0));then echo "ctr $ctr" >&$fdi;fi

	
}

function quitproper 
{
	for (( x=0;x<${#clonef[@]};x++ ));do
		kill ${clonef[$x]}
	done
	exec {fdp}<&- ; exec {fdp}>&-
	exec {fdb}<&- ; exec {fdb}>&-
	rm -rf "$bpipe"
	if ((ipid==0));then rm -rf "$pipe";fi
	imess "iquit"
	sleep 2s
	exec {fdi}<&- ; exec {fdi}>&-
	zexit 0
}

function locks
{
	xflag=0; xp=0; xpc=0;rptime=0
	((nowlocked==1)) && return
	if [ $opmode -eq 0 ]; then 
		sanityterm "$0" "sl" &
		return 1
	fi

	wai=$(whoami)
	if [ "$wai" == "root" ];then export XDG_SEAT_PATH="/org/freedesktop/DisplayManager/Seat0";fi
	
	sucloc=0
	if [[ "$lcmd" != "auto"* ]];then eval "$lcmd";sucloc=2
	elif [ "$wai" == "root" ] && timeout 0.1 loginctl lock-sessions;then sucloc=1
	elif [ "$wai" != "root" ] && timeout 0.05 loginctl lock-session;then sucloc=1
	elif dm-tool lock; then sucloc=1
	elif xfce4-screensaver-command --lock;then sucloc=1
	elif xflock4;then sucloc=1
	elif gnome-screensaver-command -l;then sucloc=1
	elif mate-screensaver-command -l; then sucloc=1
	elif xdg-screensaver lock; then sucloc=1
	elif xscreensaver-command -lock;then sucloc=1
	elif light-locker-command --lock;then sucloc=1
	elif cinnamon-screensaver-command -l;then sucloc=1
	fi
	
	if ((sucloc==1));then xset dpms force suspend
	elif ((sucloc==0));then sanityterm "$0" "lf" &	
	fi
	
}

function simlock
{
	printf "\e[104m\e[97m";clear;printf "\n\n\n\e[40m\e[K                      Simulated screen lock      \n\e[K                      Hit any key to unlock    "
	tput civis
	read -sn1 var
	echo "unlocked" >&$fdp
	exec {fdp}>&-; exec {fdp}<&-
}


function sdown
{
	zmess "sdown " &
	if [ $opmode -eq 0 ]; then
		if ((mflag==1));then
			echo -e "ShutSentry has detected that it was modified after being started.\n\nSystem shutdown would occur at this point, if running in Full mode." | xmessage -title ShutSentry -buttons Ok:0 -default Ok -file - &
		else		
			"$basex" "sdx" &>/dev/null &
		fi


		for (( x=0;x<${#clonef[@]};x++ ));do
			kill ${clonef[$x]}
		done
	else 
		if [[ $zcmd == "auto" ]];then poweroff;shutdown 0
		else 
			eval "$zcmd"
		fi
	fi

	if [ ! -z "$ipid" ] && [ ! -z "$fdi" ];then echo "quiti" >&$fdi;fi
	exec {fdp}<&- ; exec {fdp}>&- 
	exec {fdb}<&- ; exec {fdb}>&- 
	rm -rf "$pipe"
	rm -rf "$bpipe"
	zexit
}


function simdown
{
	printf "\e[101m";clear;printf "\n\n\n\e[40m\e[K                         Simulated shutdown      \n\e[K                         Hit any key to exit     "
	tput civis
	read -sn1 var &>/dev/null
	tput cnorm
	stty echo
}


function sectime
{
		declare -i hsecs=0 msecs=0 ssecs=0
		ecmd="$1"
		psecs="${ecmd//[!0-9]/}" 
		if [ "$psecs" = "$ecmd" ]; then ssecs="10#$psecs"; fi
		if [ "$psecs" = "$nil" ]; then return 1; fi

		if [[ "$ecmd" == *"h"* ]]; then
			IFS='h' read -a array <<< "$ecmd" 
			val="${array[0]}"
			val="${val//[!0-9]/}"
			val=10#$val
			hsecs=$(($val * 3600))
			ecmd="${array[1]}"
		fi
		
		if [[ "$ecmd" == *"m"* ]]; then
			IFS='m' read -a array <<< "$ecmd" 
			val="${array[0]}"
			val="${val//[!0-9]/}"
			val=10#$val
			msecs=$(($val * 60))
			ecmd="${array[1]}"
		fi

		if [[ "$ecmd" == *"s"* ]]; then
			IFS='s' read -a array <<< "$ecmd" 
			val="${array[0]}"
			val="${val//[!0-9]/}"
			val=10#$val
			ssecs=$val
			ecmd="${array[1]}"
		fi
		tsecs=$(($hsecs + $msecs + $ssecs))
		ecmd="$tsecs"
		echo "$ecmd"
	
}


function readset
{
	declare -i hsecs=0 msecs=0 ssecs=0 isecs=0
	ecmd="$2"

	if [ "$1" = "prompt" ]; then 
		prompt=$(sed 's/<br>/\\n/g'<<<$2);
		prompt=$(sed 's/<eq>/=/g'<<<$prompt)
		prompt=$(tr -d '"'<<<$prompt)
		return 1
	fi

	if [ "$1" = "lcmd" ] || [ "$1" = "tcmd" ] || [ "$1" = "zcmd" ] || [ "$1" = "oldtcmd" ] || [ "$1" = "oldzcmd" ] || [ "$1" = "oldlcmd" ]; then 
		ecmd=$(sed 's/<eq>/=/g'<<<$ecmd)
	fi

	lecmd=$(echo "$ecmd" | tr '[:upper:]' '[:lower:]')
	if [ "$ecmd" = "off" ] || [ "$ecmd" = "no" ] || [ "$ecmd" = "lock" ] || [ "$ecmd" = "test" ]; then ecmd="0";fi		
	if [ "$ecmd" = "on" ] || [ "$ecmd" = "yes" ] || [ "$ecmd" = "shutdown" ] || [ "$ecmd" = "full" ] || [ "$ecmd" = "specific" ]; then ecmd="1";fi		
	if [ "$lecmd" = "any" ]; then ecmd="2";fi	

	if [ "$1" = "pinterval" ] || [ "$1" = "cduration" ] || [ "$1" = "sdelay" ] || [ "$1" = "acounter" ]|| [ "$1" = "pcounter" ]; then

		isecs="${ecmd//[!0-9]/}"
		isecs="10#$isecs" 
		if [[ "$ecmd" == *"-"* ]];then ((isecs*=-1));fi
		tsecs=$(sectime	$ecmd)
		ecmd="$tsecs"
		if ((isecs<0));then ecmd="$isecs";fi
	fi

	case $1 in

		"pinterval") pinterval="$ecmd"  ;;
		 "usbcheck") usbcheck="$ecmd"  ;;
		"cduration") cduration="$ecmd"  ;;
		"pcounter") pcounter="$ecmd"  ;;
		"acounter") acounter="$ecmd"  ;;
		  "penalty") penalty="$ecmd"  ;;
		   "sdelay") sdelay="$ecmd" ;;
		   "lsdelay") lsdelay="$ecmd" ;;
		   "opmode") opmode="$ecmd"  ;;
		  "guard") guard="$ecmd"  ;;
		  "salt1") salt1="$ecmd"  ;;
		  "hash1") hash1="$ecmd"  ;;
		  "tcmd") tcmd="$ecmd"; if [[ $tcmd != "auto"* ]]; then oldtcmd=$tcmd;fi ;;
		  "lcmd") lcmd="$ecmd"; if [[ $lcmd != "auto"* ]]; then oldlcmd=$lcmd;fi ;;
		  "zcmd") zcmd="$ecmd"; if [[ $zcmd != "auto"* ]]; then oldzcmd=$zcmd;fi ;;
		  "oldlcmd") oldlcmd="$ecmd" ;;
		  "oldtcmd") oldtcmd="$ecmd" ;;
		  "oldzcmd") oldzcmd="$ecmd" ;;
		  "bluecheck") bluecheck="$ecmd" ;;
		  "bluedevice") bluedevice="$ecmd" ;;
	esac

	((opmode&=1)) ; ((guard&=1)) ; ((penalty&=1)) ; ((usbcheck&=1)) ; cduration="${cduration#-}"
	cduration=$((cduration<5 ? 5 : cduration))
	[[ $pcounter -eq 0 ]] && pcounter=-1
	[[ $acounter -eq 0 ]] && acounter=-1
	[[ $pinterval -eq 0 ]] && pinterval=-1
}



function iisettings
{	
	declare -i lns=0
	if ((s==0));then s=1;fi	
	s[1]=0 s[2]=1 s[3]=2 s[4]=3 s[5]=4 s[6]=5 s[7]=6 s[8]=7 s[9]=8 s[10]=9 s[11]=10 s[12]=11 s[13]=12 s[14]=13
	sv[1]=$pinterval sv[4]=$pcounter sv[5]=$acounter sv[3]=$cduration
	st[1]="$([[ $pinterval -le 0 ]] && echo "None" || echo "$(htime $pinterval)")"
	st[2]="$([[ $usbcheck = 1 ]] && echo "Yes" || echo -e "No")"
	st[3]="$(htime $cduration)"
	st[4]="$([[ $pcounter -le 0 ]] && echo "Never" || echo "$(htime $pcounter)")"
	st[5]="$(htime $acounter)"
	st[5]="$([[ $acounter -le 0 ]] && echo "Never" || echo "$(htime $acounter)")"
	st[6]="$([[ $opmode = 0 ]] && echo -e "Test      " || echo -e "Full      ")"
	st[7]="$([[ $guard = 0 ]] && echo -e "No        " || echo -e "Yes       ")"
	st[8]="Saved"
	st[9]="Stored"
	st[10]="$([[ "$tcmd" == "auto"* ]] && echo -e "Auto      " || echo -e "Custom")"
	st[11]="$([[ "$lcmd" == "auto"* ]] && echo -e "Auto      " || echo -e "Custom")"
	st[12]="$([[ "$zcmd" == "auto"* ]] && echo -e "Auto      " || echo -e "Custom")"
	st[13]="$([[ $bluecheck = 0 ]] && echo -e "No        " || ([[ $bluecheck = 2 ]] && echo -e "Any       " ||echo -e "Specific "))"

	printf "\e[40m\e[1m\e[97m"
	if [[ "$1" == *"z"* ]];then printf "\e[0;49m";fi 


	if [[ "$1" == *"z"* ]];then 
		ww=$(tput cols); ((ww-=55))
		lns=$(($lna-3))
		printf "\e[$lns;0fSettings\n--------\n\n" 
		lns=$((s[1]+$lna))
		printf "\e[$lns;"$lcol2"f %-20s" "(pinterval=$pinterval)"
		printf "\e[$((s[2]+$lna));"$lcol2"f %-20s" "(usbcheck=$usbcheck)"
		printf "\e[$((s[3]+$lna));"$lcol2"f %-20s" "(cduration=$cduration)"
		printf "\e[$((s[4]+$lna));"$lcol2"f %-20s" "(pcounter=$pcounter)"
		printf "\e[$((s[5]+$lna));"$lcol2"f %-20s" "(acounter=$acounter)"
		printf "\e[$((s[6]+$lna));"$lcol2"f %-20s" "(opmode=$opmode)"
		printf "\e[$((s[7]+$lna));"$lcol2"f %-20s" "(guard=$guard)"
		printf "\e[$((s[8]+$lna));"$lcol2"f %-20s" "(password=******)"
		printf "\e[$((s[9]+$lna));"$lcol2"f %-."$ww"s" "(prompt= $prompt)" 
		printf "\e[$((s[10]+$lna));"$lcol2"f %-."$ww"s" "(tcmd= $tcmd)"
		printf "\e[$((s[11]+$lna));"$lcol2"f %-."$ww"s" "(lcmd= $lcmd)"
		printf "\e[$((s[12]+$lna));"$lcol2"f %-."$ww"s" "(zcmd= $zcmd)"
		printf "\e[$((s[13]+$lna));"$lcol2"f %-20s" "(bluecheck=$bluecheck)"
#		printf "\e[$((s[14]+$lna));"$lcol2"f %-20s" "(bluedevice=$bluedevice)"


	fi

	
	if [[ "$1" == *"u"* ]];then
		for ((x=1;x<=13;x++));do
			lns=$((s[x]+$lna))
			printf "\e[$lns;"$lcol"f\e[1m"
			printf "%-13.23s\e[$lns;"$lcol"f" "${st[x]}"
		done
	fi

	if [[ "$1" == *"h"* ]];then
		lns=$((s[s]+$lna))
		printf "\e[$lns;"$lcol"f\e[104m\e[1m"
		printf "%-10.23s\e[$lns;"$lcol"f" "${st[s]}"
	fi

	if [[ "$1" == *"s"* ]];then
		prompt=$(sed 's/\\n/<br>/g'<<<$prompt)
		prompt=$(sed 's/=/<eq>/g'<<<$prompt)
		prompt=$(tr -d '"'<<<$prompt)
		prompt="\"""$prompt""\""
		lcmd=$(sed 's/=/<eq>/g'<<<$lcmd)
		zcmd=$(sed 's/=/<eq>/g'<<<$zcmd)
		tcmd=$(sed 's/=/<eq>/g'<<<$tcmd)
		oldlcmd=$(sed 's/=/<eq>/g'<<<$oldlcmd)
		oldtcmd=$(sed 's/=/<eq>/g'<<<$oldtcmd)
		oldzcmd=$(sed 's/=/<eq>/g'<<<$oldzcmd)
		echo "#Settings" > $INIF	
		printf -v ssave "oldzcmd=$oldzcmd\nzcmd=$zcmd\noldtcmd=$oldtcmd\noldlcmd=$oldlcmd\nlcmd=$lcmd\ntcmd=$tcmd\npcounter=$pcounter\nacounter=$acounter\nlsdelay=$lsdelay\ndpause=$dpause\nguard=$guard\nopmode=$opmode\nusbcheck=$usbcheck\npinterval=$pinterval\nsdelay=$sdelay\ncduration=$cduration\npenalty=$penalty\nzpassive=$zpassive\ndrole=$drole\nsalt1=$salt1\nhash1=$hash1\nprompt=$prompt\nFILE=$FILE\nbluecheck=$bluecheck\nbluedevice=$bluedevice\n"
		printf "$ssave" >> $INIF
		prompt=$(sed 's/<br>/\\n/g'<<<$prompt)
		prompt=$(sed 's/<eq>/=/g'<<<$prompt)
		prompt=$(tr -d '"'<<<$prompt)
		lcmd=$(sed 's/<eq>/=/g'<<<$lcmd)
		zcmd=$(sed 's/<eq>/=/g'<<<$zcmd)
		tcmd=$(sed 's/<eq>/=/g'<<<$tcmd)
		oldlcmd=$(sed 's/<eq>/=/g'<<<$oldlcmd)
		oldtcmd=$(sed 's/<eq>/=/g'<<<$oldtcmd)
		oldzcmd=$(sed 's/<eq>/=/g'<<<$oldzcmd)
	fi
	
	printf "\e[40m\e[1m\e[97m"
	if [[ "$1" == *"z"* ]];then printf "\e[0;49m";fi 


	if [[ "$1" == *"d"* ]];then
		printf "\e[$lna;0f\e[1m"
		printf "Regular interval challenge period   :  \n"
		printf "Challenge on USB device changes     :  \n"
		printf "Challenge window duration           :  \n" 
		printf "Passive failure shutdown counter    :  \n" 
		printf "Active failure shutdown counter     :  \n" 
		printf "Program core operational mode       :  \n" 
		printf "Guard main program process          :  \n"
		printf "Password for authentication         :  \n"
		printf "Challenge prompt text displayed     :  \n"   
		printf "Terminal execution command          :  \n"   
		printf "Screen lock command                 :  \n"   
		printf "System shutdown command             :  \n"   
		printf "Challenge on bluetooth dis/connect  :  \n"   
#		printf "Bluetooth device to monitor       :  \n"   
	fi	


	if [[ "$1" == *"c"* ]];then
		ln=s[1]+$lna
		printf "\e[$lna;0f\e[40m\e[1m\e[97m\e[K\n\e[K\n\e[K\n\e[K\n\e[K\n\e[K\n\e[K\n\e[K\n\e[K\n\e[K\n\e[K\n\e[K\n\e[K\n\e[K"
	fi

	if [[ "$1" != *"e"* ]];then return 1;fi
	
	while IFS="" read -rsn1 key; do

		if [ "$key" = $'\x1b' ] && read -rsn1 -t 0.01 tmp && read -rsn1 -t 0.01 tmp;then 
				case "$tmp" in
					"A") ((s--));;
					"B") ((s++));;
					"C") key=$'';;
					"D") key=$' ';;
					 "") break ;;
 					  *) break ;;
				esac
				if ((s>13)); then s=1; elif ((s<1)); then s=13;fi
				iisettings "uh"
		elif [ "$key" = $'\x1b' ];then break
		fi
		
		if [ "$key" = $'\x20' ] || [ "$key" = $'' ] || [ "$key" = $' ' ]; then
			if ((s==2)); then usbcheck=$((! usbcheck)); fi
			if ((s==6)); then opmode=$((! opmode)); fi
			if ((s==7)); then guard=$((! guard)); fi

			if ((s==13));then 
				if [ $bluecheck -eq 0 ];then bluecheck=2;iisettings "sh";tog=0;continue
				elif [ $bluecheck -eq 2 ];then bluecheck=1;iisettings "sh";tog=0;continue
				elif ((tog==1)) && [ $bluecheck -eq 1 ];then bluecheck=0;iisettings "sh";tog=0;continue
				else tog=1;blueselect;fi 
			fi


			if ((s==1)) || ((s==3)) || ((s==4)) || ((s==5));then
				((s==3)) && ctog=0
				if ((sv[s]<=0));then ((sv[s]*=-1));ctog=0;line="${sv[s]}"
				elif ((ctog));then ((sv[s]*=-1));ctog=$((1-$ctog));line="${sv[s]}"
				else 
					lns=s[s]+$lna
					tput cnorm; stty echo
					ipt="${st[s]}"
					printf "\e[40m\e[$lns;"$lcol"f             \e[$lns;"$((lcol-1))"f" 
					read -e -n9 -i "$ipt" -p ' ' line
					tput civis; stty -echo
					ctog=$((1-$ctog))
				fi
				readset $(case $s in 1) echo "pinterval" ;; 3)echo "cduration";; 4)echo "pcounter";; 5)echo "acounter";; *)echo "" ;;esac) "$line"
			fi


			if   ((s==8)) || ((s==9)) || ((s==10)) || ((s==11)) || ((s==12)); then
				lns=$lna+15
				if ((s==12));then 
					if [ "$zcmd" = "auto" ];then zcmd="$oldzcmd";iisettings "sh";tog=0;continue
					elif ((tog==1));then zcmd="auto";tog=0;iisettings "sh";continue
					else tog=1;fi 
				fi
				if ((s==11));then 
					if [ "$lcmd" = "auto" ];then lcmd="$oldlcmd";iisettings "sh";tog=0;continue
					elif ((tog==1));then lcmd="auto";tog=0;iisettings "sh";continue
					else tog=1;fi 
				fi
				if ((s==10));then 
					if [ "$tcmd" = "auto" ];then tcmd="$oldtcmd";iisettings "sh";tog=0;continue
					elif ((tog==1));then tcmd="auto";tog=0;iisettings "sh";continue
					else tog=1;fi 
				fi

				if ((s==19));then 
					if [ $bluecheck -eq 0 ];then bluecheck=2;iisettings "sh";tog=0;continue
					elif [ $bluecheck -eq 2 ] && ((tog==0));then bluecheck=1;iisettings "sh";tog=0;continue
					elif ((tog==1));then bluecheck=0;iisettings "sh";tog=0;continue
					else tog=1;fi 
				fi
				

				if ((uclear==1)); then
					clear
					((s==8)) && printf "\e[1;0f\e[41;94m\e[1m==Password==\e[40;37m"
					((s==9)) && printf "\e[1;0f\e[41;94m\e[1m==Challenge Prompt==\e[40;37m"
					((s==10)) && printf "\e[1;0f\e[41;94m\e[1m==Terminal Command==\e[40;37m"
					((s==11)) && printf "\e[1;0f\e[41;94m\e[1m==Screen Lock Command==\e[40;37m"
					((s==12)) && printf "\e[1;0f\e[41;94m\e[1m==System Shutdown Command==\e[40;37m"
					printf "\e[2;0f\e[1m\e[97m\e[43m--Enter new value and hit Enter--                 \e[0m\n"
				else
					printf "\e[$lns;0f\e[1m\e[97m\e[43m--Enter new value and hit Enter--                 \e[0m\n"
				fi
				tput cnorm; stty echo; printf "\e[40;97m"
				
				((s==8)) && read -s newvalue
				if ((s==9));then prompt=$(tr -d '"'<<<$prompt);	read -r -e -i "$prompt" newvalue;fi
				if ((s==10));then read -r -e -i "$oldtcmd" newvalue;fi
				if ((s==11));then read -r -e -i "$oldlcmd" newvalue;fi
				if ((s==12));then read -r -e -i "$oldzcmd" newvalue;fi
				tput civis; stty -echo
				
				
				if [ "$newvalue" != "" ];then
					if ((s==8));then
						tempsalt=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
						salted="$newvalue$tempsalt"
						temphash=$(echo -n "$salted" | sha256sum)
						temphash=$(tr -d '  -'<<<$temphash)
						salt1="$tempsalt" hash1="$temphash"
					elif ((s==9)); then
						prompt="$newvalue"
					elif ((s==10)); then
						tcmd="$newvalue"
						oldtcmd="$newvalue"
					elif ((s==11)); then
						lcmd="$newvalue"
						oldlcmd="$newvalue"
					elif ((s==12)); then
						zcmd="$newvalue"
						oldzcmd="$newvalue"
					fi
					[[ $uclear -eq 1 ]] && (printf "\e[2;0f\e[40m\e[K\e[45;92m\e[1m  **Updated**  \e[40;37m" && sleep 1.5s)
					if [ "${#newvalue}" -gt 100 ] || ((uclear==1)) ;then clear; iisettings "uhd";imenu 2;messi "inactive";fi
					[[ $uclear -eq 0 ]] && (lns=$lna+15;printf "\e[$lns;0f\e[40m\e[K\n\e[K\n\e[K\e[$lns;0f")
					[[ $uclear -eq 0 ]] && (printf "\e[$lns;0f\e[45;92m\e[1m  **Updated**  \e[40;37m")
					imenu 2
				else 
					((uclear==0)) && (printf "\e[42;91m\e[1m  **Unchanged**  \e[40;37m")
					[[ $uclear -eq 1 ]] && (printf "\e[2;0f\e[40m\e[K\e[44;91m\e[1m  **Unchanged**  \e[40;37m" && sleep 1.5s)
					[[ $uclear -eq 1 ]] && (clear; iisettings "uhd";imenu 2;messi "inactive")
				fi
				[[ $uclear -eq 0 ]] && sleep 1.5s 
				[[ $uclear -eq 0 ]] && (lns=$lna+15;printf "\e[$lns;0f\e[40m\e[K\n\e[K\n\e[K")
				tput civis; stty -echo
			fi

			iisettings "sh"

		elif [ "$key" = "q" ] || [ "$key" = "c" ]; then 
		break

		elif   ( ((s==1)) || ((s==3)) || ((s==4)) || ((s==5))  ) && [[ "$key" =~ [0-9] ]]; then
			lns=s[s]+$lna
			tput cnorm; stty echo
			printf "\e[40m\e[$lns;"$lcol"f             \e[$lns;"$((lcol-1))"f" 
			read -e -n9 -i "$key" -p ' ' line
			tput civis; stty -echo
			readset $(case $s in 1)echo "pinterval";; 3)echo "cduration";; 5)echo "acounter";; 4)echo "pcounter";; *)echo "" ;;esac) "$line"
			iisettings "sh "
		fi
	done
}



function imenu
{
	opc="\e[93m\e[1m" nor="\e[0m\e[44m\e[97m"	
	declare -i lns=0 
	lns=$lna-2
	((mode==5)) && upf="un" || upf=""

 	
	
	printf "\e[$lns;0f\e[44m\e[K\e[97m\e[0m\e[44m"
	case "$1" in
		3) printf "$opc S"$nor"top  un"$opc"P"$nor"ause  $opc Q"$nor"uit    $opc H"$nor"ide    $opc C"$nor"onfiguration" ;;
		2) printf "\e[43m\e[97m\e[1m Configure: \e[44m\e[93m  Up\e[97m/\e[93mDown\e[97m to select   \e[93mEnter\e[97m to change   \e[93mEscape\e[97m when done" ;;
		1) printf "$opc S"$nor"top  $opc P"$nor"ause  $opc Q"$nor"uit    $opc H"$nor"ide    $opc C"$nor"onfiguration" ;;
		0) printf "$opc S"$nor"tart $opc C"$nor"onfigure  $opc H"$nor"elp  $opc Q"$nor"uit" ;;
	esac

	printf "\e[2;0f\e[0m\e[K"
	printf "\e[30m\e[104m%-"$COLUMNS"s\e[0m" " ShutSentry v$progver"

	if [[ $hash1 == $defhash ]];then printf "\e[97m\e[104m\e[2;30f %s" "*Note: Default password 'password' is in use";fi


}

function iiconsole
{
    bname="$(basename $0)"
    TDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
    tname="$TDIR"/"$bname"

	ipipe="$tname-$unss-interface"
	ipipe=$(echo -n $ipipe | sha256sum)
	ipipe=$(tr -d '  -' <<<"$ipipe")
	ipipe="/tmp/$ipipe"
	if test -e "$ipipe";then existi=1;fi
	mkfifo "$ipipe" &>/dev/null
	exec {fdi}<> "$ipipe"

	export PS1="\e[40;92;1m[\u@\h \W]\$ \e[0;40;97m"
	printf "\e[0;40;97m"

	ww=$(tput cols)
	hh=$(tput lines)

	if [[ "$tcmd" != *"konsole"* ]] && [[ "$tcom" != *"konsole"* ]] && [ "$hh" -le 29 ]; then
		printf '\033[8;30;'"$ww"'t'
	fi

	sleep 0.1
	tlines=$(tput lines)
	[[ $tlines -lt 30 ]] && uclear=1
	
	
	clear

	declare -i imode=0 pauseask=0 ter=0
	tput civis
	stty -echo
	printf "\e[8;0f\e[104m\e[K"
	printf "\e[2;0f\e[K"
	printf "\e[30m\e[104m%-"$COLUMNS"s\e[0m" " ShutSentry v$progver"
	messi "initializing"

	if ((existi));then
		echo "hello" >&$fdi	
		sleep 1s	
		read -rs -t 0.2 greet <&$fdi
		if [ "$greet" != "hello" ];then zmess "nohello";zexit;fi
	fi

	if ((existp));then
		echo "ipid $$" >&$fdp
		if read -rs -t 1 greet <&$fdi;then messi "$greet";imenu 1;imode=1
		else messi "inactive"; imenu 0;read -rs -t 0.05 greet <&$fdp;fi
	else messi "inactive"; imenu 0
	fi

	if [ "$1" = "sc" ]; then zmess "intersc";return 1; fi


	declare -i sz=0
	stty -echo;tput civis

	while true; do

		if IFS="" read -r -s -n 1 -t 0.1 ch; then
			
			if ((pauseask==1));then
				stty erase ''
				pausetemp+="$ch"
				lns=$(($lna+1))
                bs=0
                if [ "$ch" = $'\177' ] || [ "$ch" = $'\x08' ];then bs=1;fi
				if ((bs)) && [ "${#pausetemp}" -eq 2 ]; then pausetemp="";fi
				if ((bs)) && [ "${#pausetemp}" -gt 2 ]; then sz="${#pausetemp}";((sz-=2));pausetemp=$(echo $pausetemp | cut -c1-$sz);fi
				printf "\e[$lns;0f\e[40m\e[K$pausetemp"_""
				tput civis
				if [ "$ch" == $'\x1b' ];then pauseask=0;printf "\e[$(($lns-1));0f\e[0m\e[40m\e[K\n\e[K\n\e[K";stty sane;stty -echo;fi
				if [ ! -z "$ch" ];then continue;fi
				if [ "$ch" == "" ];then 
					echo "pause $(sectime $pausetemp)" >&$fdp; pauseask=0
					printf "\e[$lns;0f\e[0m\e[40m\e[K\n\e[K";stty sane;stty -echo
				fi
				iisettings "c"
			fi

			if [ "$ch" = "q" ]; then 
				if [ "$imode" = 1 ];then echo "pause -1" >&$fdp;quiti=1 ;imode=-1
				elif [ "$imode" = 0 ];then zxmess "calmquit ";break;fi
			fi

			if [ "$ch" = "c" ]; then 
				if ((imode==0));then imenu 2;iisettings "uhed" ; iisettings "c" ;imenu 0
				else iisettings $( ((tog)) && echo "c" || echo "du");tog=$((1-tog));fi
			fi

			if [ "$ch" = "s" ]; then 
				case $imode in
					0)
						echo "initial" >&$fdp
						echo "ipid $$" >&$fdp
						setsid "$0" "start" &>/dev/null &disown
						imode=1; imenu 1;messi "starting"
						;;
					1)
						echo "pause -1" >&$fdp ;;
				esac
						
			fi

			if [ "$ch" = "p" ] && ((imode==1));then
						iisettings "c"
						lns=$(($lna-0))
						if ((lstatus==2));then
							imenu 1;echo "pause 0" >&$fdp
						else
							printf "\e[$lns;0f\e[1m\e[93m\e[40mEnter pause time: \e[0;1;40m\n"
							pausetemp=""
							pausetemp=$dpause
							pauseask=1
							pausetemp="$(htime $pausetemp)"
							printf "$pausetemp"_""
							stty erase ''
						fi
			fi

			if [ "$ch" = "h" ];then
				if ((imode==1));then clear; echo "ihide" >&$fdp;zxmess "interhide";sleep 1s;zexit;fi
				if ((imode==0));then 
					ww=$(tput cols) 
					printf "\e[40;97m";clear;ht=$(helplist);printf "$ht" | fmt --width=$ww | more -e
					printf "\e[40;97m";clear
					printf "\e[8;0f\e[104m\e[K"
					printf "\e[2;0f\e[K"
					printf "\e[30m\e[104m%-"$COLUMNS"s\e[0m" " ShutSentry v$progver"
					imenu 0;messi "inactive"
				fi
					


			fi


		fi # read anything keyinput



		if IFS=" " read -r -t 0.1 ep ep2 ep3 ep4 <&$fdi; then
			if [[ ! -z "$ep" ]]; then
				case $ep in
					"iquit") imode=0;imenu 0;messi "inactive"; if [ "$quiti" = 1 ];then zxmess "quiti1";break;fi ;;
					"hello") echo "already" >&$fdi;sleep 0.5s ;;
					"quiti") zxmess "quiti";break ;;
					      *) messi "$ep" "$ep2" "$ep3" ;; 
				esac
			fi
		fi	


	done

	rm -rf "$ipipe"
	rm -rf "$pipe"
	zxmess "interexit "
	#sleep 2s
	zexit 0

}



BASEDIR=$(dirname "$0")
unss=$(whoami)
INIF="$BASEDIR""/""$(basename "$0")""-$unss".ini
touch "$INIF"

bname="$(basename $0)"
if [ "$bname" = "Web" ];then bname="Web Content";fi
TDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
tname="$TDIR"/"$bname"
pipe="$tname-$unss"
pipe=$(echo -n $pipe | sha256sum)
pipe=$(tr -d '  -' <<<"$pipe")
pipe="/tmp/$pipe"
if test -e "$pipe";then existp=1;fi
mkfifo "$pipe" &>/dev/null
exec {fdp}<> "$pipe"

basex="$tname"

declare -i pinterval=900 cduration=30 penalty=0 sdelay=900  ctr mode newusb prevusb=0 kvm ntc alc scl pscl=1 gcl pgcl=1 kcl=1 pkcl=1 ptime=0 ectr=0 drole=1 role=0 promptcheck=0 opmode=0 ecount=0 usbcheck=0 paused=0 rptime=0 xp=0 xpc=0 clonep=0 guard=0 ctotal=0 bluecheck=0 bluef=0 pdeva=0
export ctr=10 odtitle="" pinterval=900 cwindow=15 sdelay=21600 cduration=30 cflag=0 tflag=0 xflag=0 sflag=0 mflag=0 penalty=0 zpassive=0 salt1="salt" hash1="7a37b85c8918eac19a9089c0fa5a2ab4dce3f90528dcdeec108b23ddf3607b99" 
export mode=0 lna=10 ipid=0 prompt="Authentication required" tcmd="auto" tcom="" lcmd="auto" lcom="" oldtcmd="" oldlcmd="" lastmess="" defhash=$hash1 zcmd="auto" oldzcmd="shutdown 0"
declare -i rs5c=0 tog=0 dpause=600 lsdelay=21600 sdtog=0 pcounter=21600 acounter=60 lacounter=60 ctog=0 rds=0 etime=0 otime=0 xctr=-11 lcol=40 lcol2=52 uclear=0
nil=""


ssettings=$(<"$INIF")
ssettings=${ssettings#"#Settings"}

while IFS='=' read -ra array
do
	readset "${array[0]}" "${array[1]}"
done < "$INIF"


sanityterm



#Direct command parse
if [ "$1" = "--pause" ] || [ "$1" = "pause" ]; then echo $1 $2 >&$fdp; zexit; fi
if [ "$1" = "--quit" ] || [ "$1" = "quit" ]; then echo pause -1 >&$fdp; zexit; fi
if [ "$1" = "hush" ] || [ "$1" = "--silent" ] || [ "$1" = "-s" ] || [ "$1" = "silent" ]; then 
	"$0" "start" >/dev/null 2>&1 &
	zexit
fi
if [ "$1" = "--help" ] || [ "$1" = "-h" ] || [ "$1" = "help" ]; then 
	ww=$(tput cols)
	if [ "$ww" -gt 100 ];then ww=100;fi
	ht=$(helplist);printf "$ht" | fmt --width=$ww;zexit
fi
if [ "$1" = "--set" ] || [ "$1" = "set" ]
then
	#Read existing settings from file
	while IFS='=' read -ra array
	do
		readset "${array[0]}" "${array[1]}"
	done < $INIF


	#Normal command set combo
	passtrig=0 bluetrig=0
	if [[ "$2" != *"password"* ]] && [[ "$2" != *"prompt"* ]] && [[ "$2" != *"tcmd"* ]] && [[ "$2" != *"lcmd"* ]]
	then 
		vcmd=$nil
		for (( i=2; i<=$#; i++)); do
			vcmd="${!i}"
			if [[ "$vcmd" == *"="* ]]; then
				IFS='=' read -ra array <<< "$vcmd" 
				if [[ "${array[0]}" == "password" ]]; then passtrig=1; fi
				if [[ "${array[0]}" == "bluedevice" ]]; then bluetrig=1
				else readset "${array[0]}" "${array[1]}"; fi
			else
				h=$((i+1))
				if [[ "${!i}" == "password" ]]; then passtrig=1; fi
				if [[ "${!i}" == "bluedevice" ]]; then bluetrig=1
				else readset "${!i}" "${!h}"; fi
				((i++))
			fi
		done		
	fi

	if [[ "$2" == *"lcmd"* ]]; then	
		scmd=""
		for (( i=2; i<=$#; i++))
		do scmd+="${!i} ";done
		scmd=${scmd#"lcmd="}		
		scmd=${scmd#"lcmd "}
		lcmd="$scmd"
	fi

	if [[ "$2" == *"tcmd"* ]]; then	
		scmd=""
		for (( i=2; i<=$#; i++))
		do scmd+="${!i} ";done
		scmd=${scmd#"tcmd="}		
		scmd=${scmd#"tcmd "}
		tcmd="$scmd" 
	fi

	if [[ "$2" == *"zcmd"* ]]; then	
		scmd=""
		for (( i=2; i<=$#; i++))
		do scmd+="${!i} ";done
		scmd=${scmd#"zcmd="}		
		scmd=${scmd#"zcmd "}
		zcmd="$scmd" 
	fi


	#Prompt setting command
	if [[ "$2" == *"prompt"* ]]; then
		scmd=${2#"prompt="}
		if [ "$#" -le 2 ] && [[ "$2" != *"="* ]]; then
			echo "Enter new prompt: "
			read -e -r -i "$prompt" np 
			echo $np
			scmd="$np" 
		else	
			scmd=""
			for (( i=3; i<=$#; i++))
			do
				echo " "
				scmd+="${!i} "
			done
		fi
		prompt="$scmd"
	fi

	if [[ "$2" == *"bluedevice"* ]] || [[ "$bluetrig" == 1 ]]
	then
		blueselect
	fi
	
	if [[ "$2" == *"password"* ]] || [[ "$passtrig" == 1 ]]
	then
		[[ ! -z "$3" ]] && printf "\e[1m\e[93m\e[43m***Password Change Requested***\e[0m \n"
		echo "Enter new password:"
		read -s newpass
		if [ "$newpass" != "" ]
		then
			tempsalt=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
			salted="$newpass$tempsalt"
			temphash=$(echo -n "$salted" | sha256sum)
			temphash=$(tr -d '  -'<<<$temphash)
			ssettings=${ssettings#"#Settings"}
			salt1="$tempsalt" hash1="$temphash"
			printf "\e[92m\e[1m*Password successfully changed*\e[0m\n"
			sleep 1.5s
		fi
	fi

	clear
	iisettings "sudz"
	echo " "
	zexit

fi # set command


#Default startup no arguments
if [ -z "$1" ] || [ "$1" = "--interface" ]; then  

	if ! read -r -t 0.05 mr mt mp mo me mz <&$fdp; then 
		if ((! existp));then rm -rf "$pipe";fi
		sanityterm "$0" "ii" &
		zexit 1
 	fi

    if [ "$mr" = "initial" ]; then read -r -t 0.55 mr mt mp mo me mz <&$fdp;fi
    if [ "$mr" = "ipid" ] && [ "$ipid" = 0 ]; then ipid="$mt";fi 
    if [ "$mr" != "arechief" ] && [ "$mr" != "chiefis" ] && [ "$mr" != "ipid" ]; then rm -rf "$pipe";sanityterm "$0" "ii" & zexit; fi
fi

if [ "$1" = "ii" ]; then iiconsole; zxmess "consolexit";zexit; fi
if [ "$1" = "pp" ]; then ppprompt "$1" "$2"; zexit; fi
if [ "$1" = "sl" ]; then simlock; zexit; fi
if [ "$1" = "sdx" ]; then sanityterm "$0" "sd"; zexit; fi
if [ "$1" = "sd" ]; then simdown; zexit; fi
if [ "$1" = "lf" ]; then lockfail; zexit; fi


#New clone loop
if [ "$mr" = "chiefis" ]; then
	if [ "$mr" == 'chiefis' ] && [ -z "$mainp" ] && [ ! -z "$mp" ]; then mainp="$mt"; mpipe="$mp";opmode="$mo";basex="$me";zcmd="$mz"; else zexit; fi
	if (! ps -p $mainp &>/dev/null);then rm -rf "$pipe";zexit;fi
	
	zcmd="${zcmd#'"'}"
	zcmd="${zcmd%'"'}"

	exec {fdm}<> "$mpipe"
	echo "clone $$ $0" >&$fdm
	exec {fdm}<&- ; exec {fdm}>&- ; 
	exec {fdp}<&- ; exec {fdp}>&- ; 
	rm -rf "$pipe"
	
	sleep 10s
	if (! ps -p $mainp &>/dev/null);then rm -rf "$mpipe";zexit;fi

	tmpdir=$(mktemp -d)
	tmpfi="$(basename $0)"
	mkfifo "$tmpdir/$tmpfi"

	while [[ "$(ps -p $mainp -o pid -o stat)" != *"$mainp T"* ]] && (ps -p $mainp &>/dev/null); do 
		if ! test -e "$tmpdir/$tmpfi";then sleep 1s
		else read -t 1 <>"$tmpdir/$tmpfi"; fi
	done
	rm -rf "$mpipe"
	rm -rf "$tmpdir"
	amclone=1
	sdown
fi


#Spawner
if [ "$1" = "start" ]  
then 
	echo "Program starting..."

	if ((existp));then
		echo -e "hello\"" >&$fdp
		sleep 1s
		if ! read -t 1 cvar <&$fdp;then zexit;fi
	fi


	echo "arechief" >&$fdp
	"$0" >/dev/null 2>&1 & mainp=$!

	
	if ((guard==1)); then
        sleep 4s
		ctotal=$(($RANDOM%4))
		((ctotal+=1))
		echo "ctotal $ctotal" >&$fdp
		sleep 1s
		procount="$(ps -A | awk {'print $4'} | grep -v '/' | wc -l)"
		for (( x=1;x<=$ctotal;x++ ));do
			rn=$(($RANDOM%$procount))  pnum=1
			tf=$(mktemp -d -t XXXXXXXXXXXXX)
			while read -r pline; do
				if [ "$pline" = "Web" ];then pline="Web Content";fi	
				if ((pnum==rn)); then cn="$tf/$pline";cns="$pline";break;fi
				((pnum++))
			done <<< "$(ps -A | awk {'print $4'} | grep -v '/')"
			cp "$0" "$cn"
			cpipe="$cn-$unss"
			cpipe=$(echo -n $cpipe | sha256sum)
			cpipe=$(tr -d '  -' <<<"$cpipe")
			cpipe="/tmp/$cpipe"
			mkfifo "$cpipe"
			exec {fdc}<> "$cpipe"
			echo "chiefis $mainp $pipe $opmode $basex \"$zcmd\"" >&$fdc
			"$cn" >/dev/null 2>&1 & clonep=$!
			sleep 2s
			rm -rf "$tf"
			sleep 4s
		done
		exec {fdc}<&- ; exec {fdc}>&-
	fi
	zexit
fi


if [ ! -z "$1" ];then echo "Usage:  shutsentry  [--help] [--set] [--interface] [--silent] [--quit] [--pause] ";zexit;fi


ctr=$pinterval
mainp=$$
ep=$nil
ctr=$pinterval


#Create interface pipe channel
bname="$(basename $0)"
TDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
tname="$TDIR"/"$bname"

ipipe="$tname-$unss-interface"
ipipe=$(echo -n $ipipe | sha256sum)
ipipe=$(tr -d '  -' <<<"$ipipe")
ipipe="/tmp/$ipipe"
existi=0
if test -e "$ipipe";then existi=1;fi
mkfifo "$ipipe" &>/dev/null
exec {fdi}<> "$ipipe"

#Create bluetooth info pipe channel
bpipe="$tname-$unss-btooth"
bpipe=$(echo -n $bpipe | sha256sum)
bpipe=$(tr -d '  -' <<<"$bpipe")
bpipe="/tmp/$bpipe"
mkfifo "$bpipe" &>/dev/null
exec {fdb}<> "$bpipe"

if ((existi && ! ipid));then 
	echo "starting" >&$fdi
	sleep 1s
	read -t 1 chk <&$fdi
	if [ "$chk" != "starting" ] && ((ipid==0)); then ipid=1;fi
fi


curusb=$(lsusb)
oldusb="$curusb"

imess "active"

etime=$(date +%s)
otime=$etime

chash=$(sha256sum $0)


#Main/Chief process loop

while true; do

	#Do every time

	#Read incoming messages
	sleep 0.1s
	eparray=()
	#read -r -t 0.1 -a eparray<&$fdp
	#read -r -t 0 nothing <&$fdp && read -r -t 0.1 -a eparray<&$fdp
	#if read -r -t 0 nothing <&$fdp;then read -r -t 0.1 -a eparray<&$fdp;else sleep 0.1s;fi
	read -r -t 0 nothing <&$fdp && read -r -t 0.1 -a eparray<&$fdp

	ep="${eparray[0]}"
	ep2="${eparray[1]}"
	ep3="${eparray[2]}"

	#Check timer
	etime=$(date +%s)
	rds+=1
	if ((etime!=otime)) || ((rds>=10)); then
		octr=$ctr
		ctr=$ctr-1
		if [ $(($etime - $otime)) -gt 1 ];then 
			ctr=$(($ctr-$(($etime - $otime))+1))
			if ((octr>0)) && ((ctr<0)); then ctr=0;fi
		fi
		if [ $ctr -lt 0 ]; then ctr=-5; fi
		((ctr>=0)) && zctr="$ctr" || zctr=""		
		imess "ctr" "$zctr"
		rds=0
		otime=$etime
	elif [[ -z "$ep" ]]; then
		continue
	fi
	



	#Process input for password check
	cepfull="${eparray[@]}"
	cep="${eparray[@]}"
	cep="${cep#"~~~"}"
	cep="${cep%"~~~"}"
	blahash1="$cep$salt1"
	blahash1=$(echo -n $blahash1 | sha256sum)
	blahash1=$(tr -d '  -'<<<$blahash1)


	if ((mode>0)) && [[ ${cepfull::3} == "~~~" ]] && [[ ${cepfull: -3} == "~~~" ]]; then
		for (( x=1;x<=${#cep};x++ ));do
			blahash1="${cep: -$x}$salt1"
			blahash1=$(echo -n $blahash1 | sha256sum)
			blahash1=$(tr -d '  -'<<<$blahash1)
			if [ "$blahash1" = "$hash1" ] && ((mode>0));then break;fi
		done
	fi

	
	#Interface received
	if [ "$ep" = "ipid" ] && [ "$ipid" = 0 ]; then ipid="$ep2";ep=$nil;echo "$lastmess" >&$fdi;fi
	if [ "$ep" = "ihide" ] && [ "$ipid" != 0 ]; then 
		ipid=0;xpt=0;ep=$nil
		read -t 0.1 var <&$fdi
		read -t 0.1 var <&$fdi
	fi
	if [ "$ep" = "hello\"" ];then ep=$nil;fi 

	#Clone received
	if [ "$ep" = "ctotal" ] && ((ctotal==0)); then ctotal="$ep2";ep=$nil;fi
	if [ "$ep" = "clone" ]
	then
		ep=$nil
		tclones=${#clonef[@]}
		if ((tclones<ctotal)); then 
			clonef[$tclones]="$ep2"
			clonet[$tclones]="$ep3"
		fi
	fi

	
	# Normal correct password
	if [ "$blahash1" = "$hash1" ] && ((mode>0))
	then
		zmess "corpass "
		mode=0
		ctr=$pinterval;xp=0
		ep=$nil
		if [ "$rptime" -eq -1 ]; then quitproper; fi
		if [ "$rptime" -ne 0 ]; then ctr=$rptime;rptime=0;mode=5;imess "paused";else imess "active";fi
	fi


	if [ "$ep" = "arechief" ];then ep=$nil;fi

	#Pause request
	if [[ $ep == "pause" ]] && ((mode!=0 && mode!=5));then ep=$nil;fi
	if [ "$ep" = "pause" ] && ((mode==0 || mode==5))
	then

		rptime=$(sectime $ep2)
	        if [[ $ep2 == "-1" ]];then rptime=-1;fi
		if [ "$rptime" -lt -1 ]; then rptime=0; fi
		if [ "$rptime" -eq 0 ] && ((mode==5));then mode=0;ctr=$pinterval;imess "active"; fi		
		if [ "$rptime" -gt 0 ] && ((mode==0));then cflag=1; ctr=$cduration;mode=4;imess "pausereq"; fi
		if [ "$rptime" -eq -1 ] && ((mode==0 || mode==5));then rptime=-1;cflag=1;ctr=$cduration;mode=4;imess "stopreq";fi
		ep=$nil
	fi

	#Simulated screen unlock	
	if [ "$ep" = "unlocked" ];then
		mode=2;cflag=1;tflag=1;ep=$nil;idur=1;imess "sunlocked"
	fi

	#Prompt process receive
	if [ "$ep" = "promptprocess" ];then xpt=1;xpcn="$ep2";ep=$nil;fi
	if ((xpt > 0));then ((xpt++));fi
	if ((xpt > 3)) && (ps -p "$xpcn" &>/dev/null);then xp="${xpcn//[!0-9]/}";xpt=0;xpc=0;fi
	((xpt>4)) && xpt=0;


	#Wrong password
	if [ "$ep" != "$nil" ] && [[ ${ep::3} == "~~~" ]] && [[ ${ep: -3} == "~~~" ]] && ((mode>0));then
		zmess "wrongpass "
		if ((mode==2));then tflag=1;else ctr=$acounter;fi
		mode=2; xflag=1;imess "chafail"
	fi




	#Counter to zero
	if [ $ctr -eq 0 ]
	then
		zmess "czero "
		if [ $mode -eq 2 ]; then sflag=1; fi
		if [ $mode -eq 3 ]; then sflag=1;fi
		if [ $mode -eq 4 ]; then xflag=1;mode=2;ctr=$acounter;imess "chafail";fi 
		if [ $mode -eq 1 ]; then xflag=1;mode=2;ctr=$pcounter;imess "grace";fi 
		if [ $mode -eq 0 ] && [ $pinterval -gt 0 ]; then mode=1 cflag=1 ctr=$cduration; imess "challint";fi
		if [ $mode -eq 5 ]; then ctr=$pinterval;mode=0;imess "active"; fi
	fi

	#Devices check
	if [ $usbcheck -eq 1 ]; then
		curusb=$(timeout 0.1 lsusb)
		status=$?
		if [ "$curusb" != "$oldusb" ] && [ $mode -eq 0 ] && [ $usbcheck -eq 1 ] && (( ! status ))
		then cflag=1 mode=4 ctr=$cduration; imess "usbc";fi
		if (( ! status ));then oldusb="$curusb";fi
	fi


	#Bluetooth check
	if [ $bluecheck -ge 1 ];then

		tdev="$bluedevice"
		while read -t 0 nothing <&$fdb && read -rs -t 0.05 bluecon <&$fdb;do bdevs="$bdevs$bluecon";done
		if [[ "$bdevs" == "startbcheck"*"endbcheck" ]] || [[ "$bdevs" == "" ]];then

			((bci++)) && ((bci > 5 )) && zmess "$bdevs " && ((bci=0))
			bluef=0
			if [[ "$bdevs" == *"$tdev"* ]] && [[ "$oldbdevs" != *"$tdev"* ]] && [[ "$tdev" != "" ]] && [[ "$oldbdevs" != "" ]];then bluef=2;fi
			if [[ "$bdevs" != *"$tdev"* ]] && [[ "$oldbdevs" == *"$tdev"* ]] && [[ "$tdev" != "" ]];then bluef=2;fi
			if [ $bluef -eq 2 ] && [ $mode -eq 0 ] && [ $bluecheck -eq 1 ]
			then
				bluef=0 cflag=1 mode=1 ctr=$cduration; imess "bluet"
			elif [ $mode -eq 0 ] && [ $bluecheck -eq 2 ] && [ "$bdevs" != "$oldbdevs" ] && [ "$oldbdevs" != "" ]
			then
				cflag=1 mode=1 ctr=$cduration; imess "bluet"
			fi
			oldbdevs="$bdevs"
			bdevs='startbcheck'
			((timeout 10 bluetoothctl devices Connected >&$fdb ; echo endbcheck >&$fdb) & )
		fi
	fi

	#Lock screen change check
	if [ $mode -eq 2 ]; then
		lls=$(timeout 0.1 loginctl list-sessions)
		lls2=$(echo $lls)
		lls3=$(sed 's/TTY /~/g'<<<$lls2)
		IFS='~' read -ra array <<< "$lls3"
		nlls="${#array[@]}"
		((nlls-=1))
		lls4="${array[$nlls]}"
		IFS=' ' read -ra ses <<< "$lls4"
		ses="${ses[0]}"
		tout=0
		shs=$(timeout 0.1 loginctl show-session $ses)
		if [ $? == 124 ];then tout=1;fi
		sst=$(timeout 0.1 loginctl session-status)
		if [ $? == 124 ];then tout=1;fi

		nowlocked=0

		if [[ $shs == *"IdleHint=yes"* ]];then nowlocked=1;fi	
		if [[ $shs == *"LockedHint=yes"* ]];then nowlocked=1;fi	
		ssc=$(timeout 0.05 cinnamon-screensaver-command -q)
		if [ $? == 124 ];then tout=1;fi
	   	if [[ $ssc == *"is active"* ]];then nowlocked=1;fi	    
		ssc=$(timeout 0.05 gnome-screensaver-command -q)
		if [ $? == 124 ];then tout=1;fi
		if [[ $ssc == *"is active"* ]];then nowlocked=1;fi	
		ssc=$(timeout 0.05 xfce4-screensaver-command -q)
		if [ $? == 124 ];then tout=1;fi
		if [[ $ssc == *"is active"* ]];then nowlocked=1;fi	
		ssc=$(timeout 0.05 mate-screensaver-command -q)
		if [ $? == 124 ];then tout=1;fi
		if [[ $ssc == *"is active"* ]];then nowlocked=1;fi	
		ssc=$(timeout 0.05 cinnamon-screensaver-command -q)
		if [ $? == 124 ];then tout=1;fi
		if [[ $ssc == *"is active"* ]];then nowlocked=1;fi	
		if [ $? == 124 ];then tout=1;fi
		ssc=$(timeout 0.05 light-locker-command -q)
		if [ $? == 124 ];then tout=1;fi
		if [[ $ssc == *"is active"* ]];then nowlocked=1;fi	
		if [[ $sst == *"kcheckpass "* ]];then nowlocked=1;fi 
		if [[ $sst == *"State: online"* ]];then nowlocked=1;fi

		if [[ $nowlocked == 0 ]] && [[ $prevlocked == 1 ]] && [ $mode -eq 2 ] && (( tout == 0 ))
		then mode=2; tflag=1; imess "sunlocked";fi
		prevlocked=$nowlocked
	fi

	#Clone process check
	for (( x=0;x<${#clonef[@]};x++ ));do
		if (! ps -p ${clonef[$x]} &>/dev/null);	then sflag=1;fi
		if [[ "$(ps -p ${clonef[$x]} -o pid -o stat)" == *"${clonef[$x]} T"* ]];then sflag=1;fi
	done


	#Prompt process check
	if ((xp>0))  && (! ps -p $xp &>/dev/null) && ((((xpc++)) > 3));then
		zmess "promptgone "
		if ((mode==2));then tflag=1;else ctr=$acounter;fi
		mode=2; xflag=1;imess "chafail"
	fi 


	#Interface process check
	if [ "$ipid" != 0 ] && (! ps -p $ipid &>/dev/null);then 
		zmess "intergone "
		ipid=0;xpt=0;read -t 0.1 var <&$fdi;read -t 0.1 var <&$fdi
		if ((mode==0)) || ((mode==5));then mode=1 cflag=1 ctr=$cduration ipid=0 rptime=-1;zmess "intgonechal";fi
	fi

	#App file modification check
	chashx=$(sha256sum $0)
	if [[ "$chash" != "$chashx" ]];then sflag=1;mflag=1;zmess "appfilechange ";fi





	#Flags check

	#sflag
	if [ $sflag -eq 1 ]; then sdown;fi 
	#xflag
	if [ $xflag -eq 1 ]; then 
		locks; idur=0
		if ((opmode==1));then cflag=1;idur=1;fi
	fi

	
	#cflag
	if [ $cflag -eq 1 ]; 
	then 
		cflag=0 
		sanityterm "$0" "pp" "$idur" 
		idur=0
	fi

	if [ $tflag -eq 1 ];then
		if ((ctr>acounter)) || ((ctr<0));then ctr=$acounter;fi
		imess "tflag"
		tflag=0
	fi

		
done





